\subsection{Current Network Traffic Collection Approach Overview}
The network traffic collection approach is the principal factor that enforces the outcome of the network traffic analysis. Network engineers have been collecting network traces to inspect the network traffic for numerous purposes. But most of the time, these network traces are not usable by the data scientists. Data scientists have been either utilizing very stale datasets or collecting one-shot network datasets. There have been various approaches on collecting network traffic. The most common approach is to place a data collection device on the path between users and the applications (detailed in Figure \ref{fig:simple-collection} in the introduction of this thesis).

The second most common approach to collecting network traffic and the more complex mechanism utilizes operating system capabilities to associate network packets to processes. 

Data scientists typically use these two tools: Wireshark (with a plugin called PAINT) and Microsoft Network Monitor. Although the two tools can associate packets to processes, they miss the helper packets as a means to provide valuable insights into analysis of application patterns. More importantly, the existing tools were created to monitor and troubleshoot networks with the mindset of advanced knowledge of networking rather than observation based analysis of patterns. 

Unfortunately, none of the traffic collection approaches currently used have gained enough popularity to be claimed as gold standard for the collection of network traffic data. Thus, there is a specific need for a new data collection framework.

\subsection{Data Scientists' Data Collection and Network Analysis Needs}
In the early days of the computer network, communication was performed between two participants. Each communication had a meaningful purpose and, mostly, network flow was only used for that purpose. With the improvements to network technologies, this approach to communication has changed and become more hectic. 

For example, web applications began taking advantage of content delivery networks (CDNs) for various types of content. Thus, the network traffic now includes a small subset of other applications within their application (e.g. social networks, advertisements).  Therefore, a single content retrieval for a web application now carries a set of multiple network flows. 

Effective network analysis methods require access to various data components of the network traffic. Ultimately, the data collection method must gather all the data required to complete a thorough analysis of the network traffic. This thesis focused on the top three important needs that data scientists have for effectively analyzing network traffic:
\begin{itemize}
    \item Retrieve Geographically Dispersed Data
    \item Collect High Grade Network Traffic
    \item Analyze Isolated Network Traffic
\end{itemize}

\subsubsection{Retrieve Geographically Dispersed Data}
Considering all the changes that happen on web applications, it is now common for a website retrieval to include approximately 200 network flows, as will also be verified through the developed framework in this thesis chapter \ref{label:results}.  To protect the information from malicious eyes during the network transport, most of the 200 network flows are encrypted. 

Without internal knowledge of the content retrieved, it is nearly impossible to understand the purpose of a network flow. As a result, existing data collection frameworks act oblivious to the content type and treat all network flows as equal. 

Content delivery networks are engineered towards delivering content as fast as possible to the user. For this reason, content delivery networks typically have a close point of presence to the users on the internet. Thus, each network flow of content retrieval might connect to different points of presence within the content delivery network. This behavior of content delivery networks makes it difficult to understand the network packets. Due to all of these circumstances, the data collection framework should be able to introduce artificial variance for the complete collection of all the statistical characteristics of the network flows. 

One way of collecting network data would be to simulate various network conditions in a controlled network environment. Once the simulation environment has been constructed, the collected network data would need to be verified against organic network data to verify the authenticity of the data.  This type of verification introduces more complexity to the network data. 

For these reasons, the data scientists prefer to collect authentic network data instead of collecting simulated network data. Consequently, the usage of geographically dispersed data collection points was eminent. With geographically dispersed locations, the collected network data is guaranteed to overcome of any biased network characteristics that might have been observed on only a particular location. 

Researchers can avoid focusing on limited views of the application traffic by reviewing geographically dispersed data collection points. With this approach, researchers can gather network data with a wide variety of network conditions and wide variety of reachability types.  

Thus, a new data collection framework is needed that retrieves geographically dispersed data.

\subsubsection{Collect High Grade Network Traffic}
Data science has become very popular for academic studies; however, the studies require some strict rules for the data set. Common network data does not comply with these rules. To be able to perform any comparison or any kind of data science with network traffic, there needs to be a data set that consists of the same kind of building blocks called instances. Depending on the output of the data science, instances can be defined as a packet, a network flow, or a group of flows. Traffic classification studies mostly selected a packet as an instance and used properties of each packet as variables to help determine the outcome. 

This thesis takes a higher-level approach and work on the application level.  As explained in the previous section, an example web content retrieval consists of multiple network packets, as well as multiple network flows. 

To be able to generate an application-level data set, I would have to collect high-quality data with each data instance only including the data that was created during the content retrieval. 

Computer networks are engineered for simultaneous transportation of network flows. Therefore, depending on the data collection point, network data might have multiple network flows that are not related to each other. 

For example, if network data were collected at the egress point (e.g. a router), the data instance would have multiple applications’ network traffic created from various devices.  Because of the nature of data statistics, data scientists cannot allow unrelated network data to be in the same instance. Thus, data scientists need to identify instances as content retrieval within a new data collection framework and these instances should have one device and one application per instance. 

\subsubsection{Analyze Isolated Network Traffic}
An key aspect of understanding network traffic is the need to establish the relationship between the application and the network flow. 

Various method types have been used to capture the networking data. As explained in the previous section of the thesis, each instance of data capture should contain data from only one application. This section will focus on the difficulties of capturing this isolated network data.

Data scientists use a common method to capture network traffic on the user’s device; they install a packet capture tool. This tool uses a subset of operating system functions to record the relationship between the network flow and the process. 

Since the application is also using operating system functions to resolve IP addresses from domain names, this capture method fails to capture DNS queries that belong to the applications.

While this data collection method is able to accomplish the separation of the network flows related to the application, the data collection happens on the user’s physical device with an installed agent. Since the user has to be willing to install this agent, not all users will agree to this type of data collection. Also, any network data collected from the user would need to be protected because of the participants' privacy concerns.

A new data collection framework is needed that gathers isolated network traffic data by using automated applications inside of the Linux containers. 

By leveraging Linux container technology, the developed framework is able to create a boundary between related and unrelated network traffic. This boundary ensures that the data collection framework captured only the traffic generated by the targeted application. 

Since the management of the application is realized on an out-of-band connection, there is no unnecessary network traffic that may spoil the isolated network data.