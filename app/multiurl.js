const puppeteer = require('puppeteer-core');
const fs = require('fs');

const urllist = JSON.parse(fs.readFileSync('target-urls.json', 'utf8'));

process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
  // application specific logging, throwing an error, or other logic here
});

puppeteer.launch({
  executablePath: '/usr/bin/chromium-browser',
  args: [ 
    '--disable-dev-shm-usage',
    '--no-sandbox', 
    '--disable-setuid-sandbox',
    '--disable-gpu'
  ]
}).then(async browser => {
  const promises=[];
  for(let i = 0; i < urllist.length; i++){
    console.log('Page ID Spawned', i)
    promises.push(browser.newPage().then(async page => {
      page.on('error', function (msg) { throw msg; });
      await page.setViewport({width: 1920, height: 1080});
      await page.goto(urllist[i], {
        waitUntil: 'networkidle2',
        timeout: 60000
      });
      await page.screenshot({path : '/tmp/result-'+i+'.png'});
    }).catch(function(error) {
      console.log(error.message);
    }))
  }
  await Promise.all(promises)
  browser.close();
});

// vim: ts=2 sw=2 et
