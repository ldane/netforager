#!/usr/bin/env zsh
#
PATH="$PATH:$HOME/.local/bin/"

DATA="/home/ldane/data/pcaps"
DATE=$(date +%Y%m%d)
ARGS=" --modify-window 2s --tpslimit 9 --ask-password=false -v "
RCLONE_CONFIG_PASSWORD=""

mkdir -p ${DATA}/${DATE}

rclone  ${=ARGS}  move dropbox:Apps/vts-home/ ${DATA}/${DATE}
rclone  ${=ARGS}  rmdirs --leave-root dropbox:Apps/vts-home/
