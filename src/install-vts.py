#!/usr/bin/env python

# Copyright (c) 2017  University of Houston

import argparse
import json
import logging
import os
import requests
import sys
import subprocess

from time import sleep

import geni.minigcf.config as CFG
import geni.util

import uhgeni.ssh
import uhexp.client as XC
import uhexp.util

from util import ignored, copyFile

logging.basicConfig(level = logging.INFO)
CFG.HTTP.TIMEOUT = 90

def parse_args ():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--site", dest="site", type=str)
  parser.add_argument("--slice", dest="slice", type=str)
  parser.add_argument("--project", dest="project", type=str, default="VTS-experiments")
  parser.add_argument("--context-path", dest="context_path", type=str, default=None)
  parser.add_argument("--config-path", dest="config_path", type=str, default="./vtsconfig")
  parser.add_argument("--priv-path", dest="private", type=str)

  return parser.parse_args()


if __name__ == '__main__':
  opts = parse_args()
  context = geni.util.loadContext(path = opts.context_path, key_passphrase=False)

  context.project=opts.project
  SLICE=opts.slice
  sites={}
  geni.util.updateAggregates(context, sites)
  if opts.site not in sites:
    logging.error('Site is not found')
    sys.exit(-1)
  SITE=sites[opts.site]
  logging.info('Get manifest')
  manifest = SITE.listresources(context, SLICE)
  manifest.writeXML('vts-manifest.xml')

  logging.info('Waiting sliver to get ready')
  with ignored(Exception):
    while True:
      status = SITE.sliverstatus(context, SLICE)
      if 'ready' in status['pg_status']:
        uhgeni.ssh.writeSliceConfig(opts.slice, manifest, status, path = opts.config_path, user = context.uname)
        break
      sleep(45)

  logh = uhexp.util.getLog("vts-exp.log")
  node_builder = XC.ClientFactory(logh, ssh_cfg_path = opts.config_path, manifests = [manifest])
  vts = node_builder.build("vts")

  logging.info('Installing vts-install')
  copyFile(vts,'%s/vtscfg.json' %(opts.private), '/tmp/vtscfg.json')
  copyFile(vts,'%s/vts_install' %(opts.private), '/tmp/vts_install')
  vts.ssh.run("tmux new -d 'sh /tmp/vts_install'")
  logging.info('Getting pid of vts-install')
  pid, _ = vts.ssh.run("cat /tmp/*/vts-install.pid")
  pid = int(pid)

  logging.info('Waiting for vts-install')

  with ignored(Exception):
    while vts.checkPID(pid):
      sleep(180)

  foampass, _ = vts.ssh.run('cat /etc/foam.passwd')

  vts_node = [node for node in manifest.nodes][0]
  vts_login = [login for login in node.logins][0]
  vts_host = vts_login.hostname

  logging.info('Fix VTS Tag')
  vts.ssh.run('foamctl config:set-value --passwd-file /etc/foam.passwd -k vts.geni.cm-tag -v '+ vts_host)

  url = 'https://%s:3626/core/admin/set-config' %(vts_host)
  headers = {'Content-Type': 'application/json'}
  with open("%s/dropbox-auth.json" %(opts.private)) as f:
    data = f.read()
    response = requests.post(url, data=data, headers=headers,
        auth=('foamadmin',foampass), verify=False)
  logging.info('Dropbox Added')

  ## Disable offload
  ## List interfaces
  #ip link show up|grep 'state UP'|cut -d' ' -f 2|cut -d ':' -f1
  # disable for each interface
  #ethtool --offload XXX rx off tx off sg off tso off gso off gro off

  logging.info('VTS Ready: %s' %(vts_host))

  data = {}
  data['vts-ip'] = vts_host
  data['passwd'] = foampass

  with open('vts-info.json', 'w') as f:
    json.dump(data,f)

  vts.ssh.run('sudo /etc/init.d/foam restart')

# vim: et ts=2 sw=2
