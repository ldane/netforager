#!/usr/bin/env python

import argparse
import csv
import json
import random

def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--target","-t", dest="target", type=str)
  parser.add_argument("--number","-n", default=5, dest='n', type=int)
  return parser.parse_args()


def generate_targets(target, n, ifname='top500.csv'):
  urls = []
  with open(ifname, 'rb') as f:
    reader = csv.reader(f)
    for row in reader:
      urls.append( row[1] )

  urls.remove(target)
  targets = random.sample(urls,n-1)
  targets.append(target)

  random.shuffle(targets)

  return targets

if __name__ == '__main__':
  opts = parse_args()

  targets = generate_targets(opts.target, opts.n)

  print targets
  with open('target-urls.json', 'w') as f:
      json.dump(targets,f)
