#!/usr/bin/env python

# Copyright (c) 2017  University of Houston

import argparse
import logging
import sys
import subprocess
import requests
import json

logging.basicConfig(level = logging.INFO)

import geni.aggregate.vts as VTSAM

import geni.minigcf.config as CFG
import geni.rspec.vts as VTS
import geni.rspec.pg as PG
import geni.rspec.igext as IGEX
import geni.util

import uhgeni.ssh
import uhexp.client as XC
import uhexp.util

CFG.HTTP.TIMEOUT = 90

def wait(nsec):
  import time
  time.sleep(nsec)

def parse_args ():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--site", dest="site", type=str)
  parser.add_argument("--slice", dest="slice", type=str)
  parser.add_argument("--project", dest="project", type=str, default="VTS-experiments")
  parser.add_argument("--context-path", dest="context_path", type=str, default=None)
  parser.add_argument("--config-path", dest="config_path", type=str, default="./vtsconfig")
  parser.add_argument("--type", dest="restype", type=str, default="pc")
  parser.add_argument("--create", dest="create", action="store_true", default=False)

  return parser.parse_args()


if __name__ == '__main__':
  opts = parse_args()
  context = geni.util.loadContext(path = opts.context_path, key_passphrase=False)

  context.project=opts.project
  SLICE=opts.slice
  sites={}
  geni.util.updateAggregates(context, sites)
  if opts.site not in sites:
    logging.error('Site is not found')
    sys.exit(-1)
  SITE=sites[opts.site]

  if opts.create:
    slices=context.cf.listSlices(context)
    for sliceurn, slicedata in slices.items():
      if slicedata['SLICE_NAME'] == SLICE:
        if not slicedata['SLICE_EXPIRED']:
          opts.create=False
          logging.info('Slice Found')
        else:
          logging.info('Slice found but expired')

  if opts.create:
    context.cf.createSlice(context, SLICE)

  r = PG.Request()

  if opts.restype == 'pc':
    pc=PG.RawPC('vts')
    pc.disk_image='+'.join(SITE.component_manager_id.split('+')[0:2]+['image','emulab-ops:UBUNTU18-64-STD'])

    r.addResource(pc)
  else:
    vm = IGEX.XenVM('vts')
    vm.disk_image='+'.join(SITE.component_manager_id.split('+')[0:2]+['image','emulab-ops:UBUNTU18-64-STD'])

    r.addResource(vm)
  r.writeXML('vts-request.xml')

  geni.util.deleteSliverExists(SITE,context,SLICE)

  logging.info('Creating the sliver')
  manifest = SITE.createsliver(context, SLICE, r)
  manifest.writeXML('vts-manifest.xml')

# vim: et ts=2 sw=2
