#!/usr/bin/env python

# Copyright (c) 2017  University of Houston

import argparse
import logging
import sys
import subprocess
import requests
import json

logging.basicConfig(level = logging.INFO)

import geni.aggregate.vts as VTSAM
import geni.minigcf.config as CFG
import geni.rspec.vts as VTS
import geni.util

import uhgeni.ssh

CFG.HTTP.TIMEOUT = 90

def parse_args ():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--uhexpbranch", dest="uhexpbranch", type=str, default="default")
  parser.add_argument("--uhexpurl", dest="uhexpurl", type=str, default="https://bitbucket.org/uh-netlab/uhexp")
  parser.add_argument("--slice", dest="slice", type=str)
  parser.add_argument("--project", dest="project", type=str, default="VTS-experiments")
  parser.add_argument("--context-path", dest="context_path", type=str, default=None)
  parser.add_argument("--config-path", dest="config_path", type=str, default="./expconfig")
  parser.add_argument("--delete", dest="delete", action="store_true", default=False)
  parser.add_argument("--url-file", dest="urlfile", type=str)
  parser.add_argument("--use-tabs", dest="tabs", action="store_true", default=False)

  return parser.parse_args()


def conNodes (r, uhexpbranch, uhexpurl, name = "client"):
  appurl = "https://bitbucket.org/ldane/netuser-app"
  appbranch = "default"
  client = r.Container(VTS.Image("netuser.puppy"), name)
  client.HgMount("uhexp", uhexpurl, "/uhexp", uhexpbranch)
  client.HgMount("netuser-app", appurl, "/app", appbranch)
  client.DropboxMount("clientResults", "/exp-data")
  return

def buildRequest (opts):
  r = VTS.Request()
  urls = []
  with open(opts.urlfile) as f:
    urls = json.load(f)

  n = len(urls)
  if opts.tabs:
    n = 1

  for i in range(n):
    conNodes(r, opts.uhexpbranch, opts.uhexpurl, name="client%02d" %(i))

  r.writeXML("request.xml")
  logging.info("Request saved at request.xml")
  return r

def loadVTSInfo ():
  if not hasattr(loadVTSInfo, "vts_host"):
    with open('vts-info.json') as f:
      vts_info = json.load(f)
      loadVTSInfo.vts_host = vts_info['vts-ip']
      loadVTSInfo.foampass = vts_info['passwd']
  return (loadVTSInfo.vts_host, loadVTSInfo.foampass)


def attachNATInterface (m):
  logging.info("Attaching NAT Interface")

  vts_host, foampass = loadVTSInfo()

  #Assumes manifest has only one sliver
  sliver_id = [container.sliver_id for container in m.containers][0]
  url = 'https://%s:3626/core/admin/vts/sliver/%s/containers' %(vts_host, sliver_id)
  response = requests.get(url, auth=('foamadmin',foampass), verify=False)
  response = json.loads(response.text)
  containers = {c['client_id']:c['id'] for c in response['value']}

  ip = 100
  for cname,cid in containers.items():
    url = 'https://%s:3626/core/admin/vts/container/%s/nat' %(vts_host, cid)
    data='{"ip-address" : "172.17.0.%d/16", "gateway" : "172.17.0.1"}' %(ip)
    ip += 1
    headers = {'Content-Type': 'application/json'}
    response = requests.put(url, data=data, headers=headers, auth=('foamadmin',foampass), verify=False)
  logging.info("Attached NAT Interface")


if __name__ == '__main__':
  opts = parse_args()

  context = geni.util.loadContext(path = opts.context_path, key_passphrase=False)
  context.project = opts.project

  logging.info("Getting foam passwd")
  vts_host, foampass = loadVTSInfo()
  am = VTSAM.VTS('LD_VTS', vts_host)

  if opts.delete:
    logging.info("Deleting existing sliver")
    try:
      am.deletesliver(context, opts.slice)
    except:
      logging.info("Error deleting sliver")

  logging.info("Building request")
  r = buildRequest(opts)

  logging.info("Calling createsliver")
  m = am.createsliver(context, opts.slice, r)
  m.writeXML("manifest.xml")
  logging.info("Manifest saved at manifest.xml")

  attachNATInterface(m)

  uhgeni.ssh.writeSliceConfig(opts.slice, m, path = opts.config_path)
  logging.info("ssh config file saved at %s" % (opts.config_path))

# vim: et ts=2 sw=2
