#!/usr/bin/env python

import random

from multiprocessing.dummy import Pool

import geni.util

def checkSite(args):
  s,o = args
  if '-ig' not in s:
    return (s,False)
  ad=None
  try:
    ad=o.listresources(context)
  except:
    return (s,False)
  for n in ad.nodes:
    if 'pc' in n.hardware_types and n.available:
      return (s,o)
  return (s,False)

if __name__ == '__main__':

  context = geni.util.loadContext(key_passphrase=True)

  sites={}
  geni.util.updateAggregates(context,sites)

  pool = Pool(8)
  sites = pool.map(checkSite, sites.items())
  pool.close()
  pool.join()

  sites = {s:o for s,o in sites if o}

  print "\n".join(sites.keys())
