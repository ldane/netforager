from contextlib import contextmanager
from uhexp.client.core.clientfactory import IGClient, VTSClient

@contextmanager
def ignored(*exceptions):
  try:
    yield
  except exceptions:
    pass


def getSFTP(node):
  if node.ssh._transport is None:
    node.ssh.connect()
  sftp = node.ssh.client.open_sftp()
  return sftp

def copyFile(node,localFile,remoteFile):
  if isinstance(node,IGClient):
    if not hasattr(node,'_sftp'):
      node._sftp = getSFTP(node)
    node._sftp.put(localFile, remoteFile)
  elif isinstance(node,VTSClient):
    raise Exception('Nope')
    pass
    #client.ssh.run("cat <<EOF >/target-urls.json\n" + urls + "\nEOF")

# vim: et ts=2 sw=2
