#!/usr/bin/env python
# Copyright (c) 2017  University of Houston

# Reference example of fully orchestrated experiment using UHEXP tools

import argparse
import datetime as dt
import json
import logging
import sys

from time import sleep
from urlparse import urlparse

import uhexp.client as XC
import uhexp.util

from geni.aggregate.vts import VTS

def work(clients, targets, tabs=False):
  ####################
  # Experiment Setup #
  ####################

  # We need to install uhexp everywhere
  for c, node in clients.items():
    XC.Base(node['client']).install()
    sleep(2)

  # Wait for install to finish
  XC.waitForJobs()

  for c, node in clients.items():
    XC.SysInfo(node['client']).getOS()
    sleep(2)

  # Get handles to the orchestration tools for the apps we need to use, bound
  # to the nodes we're going to use them on
  tkl = []

  for c, node in clients.items():
    node['tcpdump'] = XC.TCPDump(node['client'])
    # Start all the required installs
    tkl.append(node['tcpdump'].install())
    sleep(2)

  # Wait for the installs to complete
  XC.waitForJobs()

  # Utility function to end script if a token is marked failed
  uhexp.util.checkAndDie(tkl)

  # Collect OS info from all the nodes
  for c, node in clients.items():
    XC.UHGetConf(node['client']).run()
    sleep(2)

  XC.waitForJobs()

  ##################
  # Experiment Run #
  ##################

  if tabs:
    data = [(client[0], targets)]
  else:
    data = zip(clients, targets)

  pids = []
  for cname, target in data:
    node = clients[cname]
    if not isinstance(target, list):
      target = [target]

    urls = json.dumps(target)
    tag = '-'.join([urlparse(url).netloc for url in target])
    # Write targets
    node['client'].ssh.run("cat <<EOF >/target-urls.json\n" + urls + "\nEOF")

    # Start tcpdump - does not return a job token
    node['tcpdump'].start(interface="eth0", ofpath="/exp-data/capture-%s.pcap" % (tag))

    # Run the puppeteer
    node['client'].ssh.run("tmux new -d 'node /app/multiurl.js'")
    pid, _ = node['client'].ssh.run("pgrep node")
  # pid, _ = client.ssh.run("cat /tmp/node.pid")
    pids.append((node['client'],int(pid), dt.datetime.now()))
    sleep(2)

  # give clients some time to finish the job
  sleep(10)

  # Wait until all the browsers are completed
  while len(pids) > 0:
    (client, pid, ts) = pids.pop(0)
    if client.checkPID(pid):
      # It's been 5 minutes kill it!
      if dt.datetime.now() - ts > dt.timedelta(minutes=5):
        client.ssh.run("kill -TERM %d" % (pid))
      # Append back to queue
      pids.append((client, pid, ts))
    sleep(2)

  # Wait for connections to finish
  sleep(10)

  XC.waitForJobs()

  # Stop tcpdump
  for c, node in clients.items():
    node['tcpdump'].stop()

  #######################
  # Get Experiment Data #
  #######################

  for c, node in clients.items():
    node['client'].ssh.run("cp /target-urls.json /exp-data/")
    node['client'].downloadData(local_root="./exp-data/")
    sleep(10)

  # Wait for the transfer(s) to finish
  XC.waitForJobs()

def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--use-tabs", dest="tabs", action="store_true", default=False)
  parser.add_argument("--url-file", dest="urlfile", type=str)

  return parser.parse_args()

def buildClients():
  vts_am = None

  logh = uhexp.util.getLog("ref-exp.log")
  node_builder = XC.ClientFactory(logh, ssh_cfg_path="expconfig", manifests=["manifest.xml"])

  clients = {}
  for client in node_builder._client_map:
    node = {}
    node['client'] = node_builder.build(client)
    if vts_am == None:
      vts_am = VTS('tag', node_builder._client_map[client][3])
    node['client'].site = vts_am

    clients[client] = node

  return clients


if __name__ == '__main__':
  opts = parse_args()
  #Load target list from target.json
  targets = []
  with open(opts.urlfile) as f:
    targets = json.load(f)

  ######################
  # Orchestrator Setup #
  ######################

  # Get client objects for each node, bound to the required infrastructure specialization (VTS, in this case)
  clients = buildClients()

  # do work
  work(clients, targets, opts.tabs)
