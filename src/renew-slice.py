#!/usr/bin/env python

import datetime as dt
import os

import geni.util


SLICE = 'ldane-vts'
EXP = dt.date.today() + dt.timedelta(days=7)

CONTEXT = geni.util.loadContext()

sites = {}
geni.util.updateAggregates(CONTEXT, sites)

CONTEXT.cf.renewSlice(CONTEXT, SLICE, EXP)

for site in os.listdir('.'):
  sites[site].renewsliver(CONTEXT, SLICE, EXP)
