#!/usr/bin/env sh
if [ ! -f /tmp/top500.csv ]; then
  echo 'top500 not found, downloading...'
  curl -s https://moz.com/top500/domains/csv >/tmp/top500.csv
fi

cp /tmp/top500.csv ./
