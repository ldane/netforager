#!/usr/bin/env python
# coding: utf-8
import argparse
import csv
import itertools
import os, errno

#import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

import numpy as np
import seaborn as sns
import scipy.stats

from collections import Counter
from datetime import datetime,timedelta
from tabulate import tabulate

import process
from utils import *


SITE_LOOKUP = {
  'pc3.instageni.utc.edu': (0,'Chattanooga-TN'),
  'pc5.instageni.cenic.net': (1,'Los Angeles-CA'),
  'pc5.instageni.wisc.edu': (2,'Madison-WI'),
  'pc5.instageni.hawaii.edu': (3, 'Honolulu-HI'),
  'pc5.genirack.nyu.edu': (4,'New York-NY')
  }

def plotConvCount(fig, data):
  pcap_data, stats = data

  gs = fig.add_gridspec(2,2, height_ratios= [3,1])
  ax0 = fig.add_subplot(gs[0,:])

  counts = [(len(data['convs']), len(data['processed'])) for pcap_file, data in pcap_data.items()]
  counts.sort(reverse=True)

  convCount = [len(data['convs']) for pcap_file, data in pcap_data.items()]
  processedCount = [len(data['processed']) for pcap_file, data in pcap_data.items()]

  ax0.plot(convCount, '.', label='Raw Convs')
  ax0.plot([np.mean(convCount)]*len(pcap_data), label='Avg(Raw)')

  ax0.plot(processedCount, '.', label='Copies')
  ax0.plot([np.mean(processedCount)]*len(pcap_data), label='Avg(Copies)')
  ax0.legend()
  ax0.set_title('Item Count')
  ax0.set_xlabel('Pcap Instance')
  ax0.set_ylabel('Conversation Count')

  ax1 = fig.add_subplot(gs[1,0])
  plotRawConvHist(ax1, data)

  ax2 = fig.add_subplot(gs[1,1])
  plotProConvHist(ax2, data)

  fig.tight_layout()


def plotCDFConvs(fig, data):
  pcap_data, stats = data

  ax = fig.gca()

  maxT = 0.

  site_convs = {}

  for fname,v in pcap_data.items():
    site_convs.setdefault(v['site'], [])
    site_convs[v['site']].append(len(v['convs']))

  for site,v in site_convs.items():
    ax.plot(np.sort(v), np.linspace(0, 1, len(v), endpoint=False), label=SITE_LOOKUP[site][1], marker='.', lw=0)
    maxT = max(maxT, max(v))

  ax.set_title('CDF of Conv Count')
  ax.set_xlabel('Conversation Count')
  ax.set_ylabel('Cumulative Probability')
  #ax.set_xlim(0,maxT)
  ax.legend()


def plotCDFBandwidth(fig, data):
  pcap_data, stats = data

  ax = fig.gca()

  maxT = 0.

  site_band = {}

  for fname,v  in pcap_data.items():
    site = v['site']

    total = sum([conv['len'] for conv in v['convs']])

    site_band.setdefault(site, [])
    site_band[site].append( total/v['dur'] )

  for site,v  in site_band.items():
    ax.plot(np.sort(v), np.linspace(0, 1, len(v), endpoint=False), label=SITE_LOOKUP[site][1], marker='.', lw=0)
    maxT = max(maxT, max(v))

  ax.set_title('CDF of Bandwidth')
  ax.set_xlabel('Bandwidth')
  ax.set_ylabel('Cumulative Probability')
  #ax.set_xlim(0,maxT)
  ax.xaxis.set_major_formatter(ticker.FuncFormatter(format_bytes))
  ax.legend()


def plotCDFTime(fig, data, polyfit=False):
  pcap_data, stats = data

  ax = fig.gca()

  maxT = 0.
  for site,v in stats['site_dur'].items():
    x=np.sort(v)
    y=np.linspace(0, 1, len(v), endpoint=False)
    ax.plot(x, y, label=SITE_LOOKUP[site][1], marker='.', lw=0)
    if polyfit:
      trend=np.polyfit(y, x, deg=1)
      trendpoly = np.poly1d(trend)
      ax.plot(trendpoly(y), y, label=SITE_LOOKUP[site][1])
    maxT = max(maxT, max(v))

  ax.set_title('CDF of Duration')
  ax.set_xlabel('Duration (s)')
  ax.set_ylabel('Cumulative Probability')
  ax.set_xlim(0,maxT)
  ax.legend()


def cdfTimeHelper(fig, data, f):
  ax = fig.gca()
  f_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
  f_writer.writerow(['app', 'site', 'min', 'max', 'mean', 'std', 'midpoint', 'slope', 'intercept', 'linerr', 'mu', 'sigma', 'normerr'])

  for site,v in data.items():
    color = 'C%d' %(SITE_LOOKUP[site][0])
    x, y, popt, pcov, lin, f = curve_fit(v)

    ax.plot(x,y, color=color, marker='.', alpha=0.2, lw=0) #, label=SITE_LOOKUP[site][1])

    ax.plot([-lin.intercept/lin.slope, (1-lin.intercept)/lin.slope], [0,1], '--', color=color) #, label='mean:%lf slope:%lf' %((1-2*lin.intercept)/(2*lin.slope),lin.slope))

    ax.plot(x, f(x, *popt), color=color, label=SITE_LOOKUP[site][1])

    err = y - f(x, *popt)
    err = np.sqrt(np.mean(err**2))

    f2 = lambda x, a, b: a*x + b
    err2 = y - f2(x, lin.slope, lin.intercept)
    err2 = np.sqrt(np.mean(err2**2))

    midpoint = (0.5 - lin.intercept) / lin.slope

    f_writer.writerow([tag, SITE_LOOKUP[site][1], min(x), max(x), np.mean(x), np.std(x), midpoint, lin.slope, lin.intercept, err2, *popt, err])

  ax.set_xlabel('Duration (s)')
  ax.set_ylabel('Cumulative Probability')
  ax.legend()


def genResultTable(fig, data):
  pcap_data, stats = data

  import os.path
  with open('/home/ldane/Desktop/results.csv', mode='a') as f:
    new_file = False
    if f.tell() == 0:
      new_file = True
    f_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    if new_file:
      f_writer.writerow([
        'app',
        'con_length',
        'iqr1',
        'conv_count',
        'iqr2',
        'durfit_cha',
        'durfit_la',
        'durfit_mad',
        'durfit_hon',
        'durfit_ny',
        'durfit_std',
        '15kfit_cha',
        '15kfit_la',
        '15kfit_mad',
        '15kfit_hon',
        '15kfit_ny',
        '15kfit_std'
      ])

    sites = ['cha', 'la', 'mad', 'hon', 'ny']

    durfit = {}
    f15kfit = {}

    for site,v in stats['site_dur'].items():
      x, y, popt, pcov, lin, f = curve_fit(v)
      site_tag = sites[SITE_LOOKUP[site][0]]

      durfit[site_tag] = popt[1]

    for site,v in calculate15k(data).items():
      x, y, popt, pcov, lin, f = curve_fit(v)
      site_tag = sites[SITE_LOOKUP[site][0]]

      f15kfit[site_tag] = popt[1]


    conv_count = []
    cont_len = []
    for fname, v in pcap_data.items():
      conv_count.append(len(v['convs']))
      cont_len.append(v['len'])

    row = [
        tag,
        format_bytes(np.mean(cont_len)),
        format_bytes(scipy.stats.iqr(cont_len, interpolation='midpoint')),
        round(np.mean(conv_count)),
        scipy.stats.iqr(conv_count, interpolation='midpoint')
    ]

    for s in sites:
      row.append(durfit[s])
    row.append(np.std(list(durfit.values())))

    for s in sites:
      row.append(f15kfit[s])
    row.append(np.std(list(f15kfit.values())))

    f_writer.writerow(row)


def plotWeeklyData(fig, data):
  pcap_data, stats = data

  sites = ['cha', 'la', 'mad', 'hon', 'ny']

  weeks = {}
  for i in range(12):
    weeks[i] = {}

  for fname, v in pcap_data.items():
    dt = datetime.fromtimestamp(v['start']) - datetime(2019, 8, 6)
    w = int(dt.days/7)

    weeks[w].setdefault(v['site'], {})[fname] = v

  del(weeks[11])


  metrics = {}
  for w in weeks:
    metrics[w] = {}
    for s in sites:
      metrics[w][s] = {'cont_len': [], 'conv_cnt': [], 'k':0, 'x0':0}

  for w,sites_data in weeks.items():
    for site,files in sites_data.items():
      site_tag = sites[SITE_LOOKUP[site][0]]

      dur_data = [v['dur'] for f,v in files.items()]
      x, y, popt, pcov, lin, f = curve_fit(dur_data)
      metrics[w][site_tag]['k'] = popt[1]
      metrics[w][site_tag]['x0'] = popt[0]

      for f,v in files.items():
        metrics[w][site_tag]['cont_len'].append(v['len'])
        metrics[w][site_tag]['conv_cnt'].append(len(v['convs']))

  cfig = plt.figure()
  ax = cfig.gca()

  for i,s in enumerate(sites):
    sname = None
    for k,v in SITE_LOOKUP.items():
      if v[0] == i:
        sname = v[1]

    ax.plot(range(1,12), [metrics[w][s]['k'] for w in metrics], label=sname)

  t = ax.get_ylim()
  ax.set_ylim(0, t[1]+t[0])
  ax.set_xticks(range(1,12))
  ax.set_xlabel('Weeks')
  ax.set_ylabel('Consistency')
  _ = ax.legend()

  cfig.savefig(f"{PLOTFOLDER}/cons.png", bbox_inches="tight")


  figs = {}
  for i,s in enumerate(sites):
    sname = None
    for k,v in SITE_LOOKUP.items():
      if v[0] == i:
        sname = v[1]
    figs[s] = plt.figure()
    ax = figs[s].gca()

    ax.boxplot([metrics[w][s]['conv_cnt'] for w in metrics], showfliers=False)

    ax.set_xlabel('Weeks')
    ax.set_ylabel('Conversation Count')
    ax.set_title(sname)

  minY = 2**32
  maxY = 0
  for s,cfig in figs.items():
    ax = cfig.gca()
    y = ax.get_ylim()
    minY = min(minY, y[0])
    maxY = max(maxY, y[1])

  for s,cfig in figs.items():
    ax = cfig.gca()
    ax.set_ylim(minY*0.85,maxY*1.15)
    cfig.savefig(f"{PLOTFOLDER}/conv-{s}.png", bbox_inches="tight")

  figs = {}
  for i,s in enumerate(sites):
    sname = None
    for k,v in SITE_LOOKUP.items():
      if v[0] == i:
        sname = v[1]

    figs[s] = plt.figure()
    ax = figs[s].gca()

    ax.boxplot([metrics[w][s]['cont_len'] for w in metrics], showfliers=False)
    ax.yaxis.set_major_formatter(ticker.FuncFormatter(format_bytes))

    ax.set_xlabel('Weeks')
    ax.set_ylabel('Content Length')
    ax.set_title(sname)


  minY = 2**32
  maxY = 0
  for s,cfig in figs.items():
    ax = cfig.gca()
    y = ax.get_ylim()
    minY = min(minY, y[0])
    maxY = max(maxY, y[1])

  for s,cfig in figs.items():
    ax = cfig.gca()
    ax.set_ylim(minY*0.85,maxY*1.15)
    cfig.savefig(f"{PLOTFOLDER}/cont-{s}.png", bbox_inches="tight")







def plotCDFTimeFit(fig, data):
  pcap_data, stats = data

#  with open('/home/ldane/Desktop/linfit.csv', mode='a+') as f:
#    cdfTimeHelper(fig, stats['site_dur'], f)
  with open('/home/ldane/Desktop/%s/linfit.csv' %(tag), mode='w') as f:
    cdfTimeHelper(fig, stats['site_dur'], f)

  ax = fig.gca()
  ax.set_title('Retrieval Time')

def calculate15k(data, length_limit = 15000):
  pcap_data, stats = data
  site_15kdur = {}
  for fname, data in pcap_data.items():
    total = 0
    start = data['convs'][0]['values'][0][0]
    convs = []

    for c in data['convs']:
      for cp in c['values']:
        convs.append(cp)

    for conv in sorted(convs, key=lambda x: x[0]):
      total+=conv[1]
      end = conv[0]
      if total > length_limit:
        break
    if end - start < 0:
      print(fname)

    site_15kdur.setdefault(data['site'], []).append(end-start)

  return site_15kdur


def plotCDF15kTimeFit(fig, data, length_limit=15000):
  pcap_data, stats = data

#  with open('/home/ldane/Desktop/site15k-fit.csv', mode='a+') as f:
#    cdfTimeHelper(fig, calculate15k(data), f)
  with open('/home/ldane/Desktop/%s/site15k-fit.csv' %(tag), mode='w') as f:
    cdfTimeHelper(fig, calculate15k(data), f)

  ax = fig.gca()
  ax.set_title('Initial PageLoad')


def plotCDFLenFit(fig, data):
  pcap_data, stats = data

  ax = fig.gca()

  maxT = 0.
  #with open('/home/ldane/Desktop/len-fit.csv', mode='a+') as f:
  #  f_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
  for site,v in stats['site_len'].items():
    color = 'C%d' %(SITE_LOOKUP[site][0])

    x, y, lin, xL = linFit(v, True)

    ax.plot(x, y, color=color, label=SITE_LOOKUP[site][1], marker='.', alpha=0.6, lw=0)

  #    f_writer.writerow([tag, SITE_LOOKUP[site][1], min(x), max(x), lin.slope, lin.intercept])

  ax.set_title('Content Length')
  ax.set_xlabel('Total Bytes')
  ax.set_ylabel('Cumulative Probability')
  #ax.set_xlim(0,maxT)
  ax.xaxis.set_major_formatter(ticker.FuncFormatter(format_bytes))
  ax.legend()

def plotCDFLen(fig, data):
  pcap_data, stats = data

  ax = fig.gca()

  maxT = 0.
  for site,v in stats['site_len'].items():
    ax.plot(np.sort(v), np.linspace(0, 1, len(v), endpoint=False), label=SITE_LOOKUP[site][1], marker='.', lw=0)
    maxT = max(maxT, max(v))

  ax.set_title('CDF of Total Bytes')
  ax.set_xlabel('Total Bytes')
  ax.set_ylabel('Cumulative Probability')
  ax.set_xlim(0,maxT)
  ax.xaxis.set_major_formatter(ticker.FuncFormatter(format_bytes))
  ax.legend()


def plotRawConvHist(ax, data):
  pcap_data, stats = data
  convCount = [len(data['convs']) for pcap_file, data in pcap_data.items()]

  x = ax.hist(convCount)
  ax.set_xticks(x[1])
  ax.set_title('Histogram of Raw')
  ax.set_xlabel('Conversation Count')
  ax.set_ylabel('Instances')


def plotProConvHist(ax, data):
  pcap_data, stats = data
  processedCount = [len(data['processed']) for pcap_file, data in pcap_data.items()]

  x = ax.hist(processedCount, color='g')
  ax.set_xticks(x[1])
  ax.set_title('Histogram of Processed')
  ax.set_xlabel('Conversation Count')
  ax.set_ylabel('Instances')


def plotConvLine1(fig, data):
  pcap_data, stats = data
  counts = [(len(data['convs']), len(data['processed'])) for pcap_file, data in pcap_data.items()]
  counts.sort(reverse=True)

  ax0 = fig.add_subplot(2,1,1)

  ax0.plot([i[0] for i in counts], label='Raw Convs')
  ax0.plot([i[1] for i in counts], label='Processed')
  ax0.set_xlabel('Instances')
  ax0.set_ylabel('Conversation Count')

  ax1 = fig.add_subplot(2,1,2)

  ax1.plot([i[0] for i in counts], label='Raw Convs')
  ax1.plot(sorted([i[1] for i in counts],reverse=True), label='Processed')
  ax1.set_xlabel('Instances')
  ax1.set_ylabel('Conversation Count')


def plotConvLine2(fig, data):
  pcap_data, stats = data

  binCount = 10

  convCount = Counter([len(data['convs']) for pcap_file, data in pcap_data.items()]).most_common()

  processedCount = Counter([len(data['processed']) for pcap_file, data in pcap_data.items()]).most_common()

  ax0 = fig.add_subplot(2,1,1)

  ax0.plot([i[1] for i in convCount])
  ax0.set_xticks(range(len(convCount)))
  ax0.set_xticklabels([str(i[0]) for i in convCount], rotation='vertical', fontsize=8)
  ax0.set_xlabel('Conversation Count')
  ax0.set_xlabel('Instances')

  ax1 = fig.add_subplot(2,1,2)

  ax1.plot([i[1] for i in processedCount])
  ax1.set_xticks(range(len(processedCount)))
  ax1.set_xticklabels([str(i[0]) for i in processedCount], rotation='vertical', fontsize=8)
  ax1.set_xlabel('Conversation Count')
  ax0.set_xlabel('Instances')


def plotConcurrent(fig, data):
  pcap_data, stats = data

  gs = fig.add_gridspec(2,2, height_ratios= [3,1])
  ax0 = fig.add_subplot(gs[0,:])

  duplicates = {}

  for pcap_ind, (pcap_file, data) in enumerate(pcap_data.items()):
    cnt = 0
    for ind, row in enumerate(data['processed']):
      if row[1] == 1:
        continue
      duplicates.setdefault(row[1], {'x':[], 'y':[]})
      duplicates[row[1]]['x'].append(pcap_ind)
      duplicates[row[1]]['y'].append(ind)


  for i in duplicates:
    ax0.plot(duplicates[i]['x'], duplicates[i]['y'], color=next(ax0._get_lines.prop_cycler)['color'], marker='.', linestyle="None", label='Index of %ds' %(i))
  ax0.legend()
  ax0.set_xlabel('Pcap Index')
  ax0.set_ylabel('Duplicate Position in Pcap')

  ax2 = fig.add_subplot(gs[1,0])
  res = []
  for k,v in duplicates.items():
    for t in v['x']:
      res.append(k)
  x = ax2.hist(res)
  ax2.set_xticks(x[1])
  ax2.set_title('Histogram of duplicates')
  ax2.set_xlabel('Duplicate Count')
  ax2.set_ylabel('Instance Count')

  ax3 = fig.add_subplot(gs[1,1])
  sizes = []
  labels = []
  for i in duplicates:
    labels.append(i)
    sizes.append(len(duplicates[i]['x']))

  ax3.pie(sizes, labels=labels)
  ax3.axis('equal')
  ax3.set_title('Pie of duplicates')

  #ax4 = fig.add_subplot(gs[2,0])
  #res = []
  #for k,v in duplicates.items():
  #  for t in v['x']:
  #    res.append(t)

  #ax5 = fig.add_subplot(gs[2,1])
  fig.tight_layout()


def pcapTimeline(ax, data = None, pcap_data = None, ncol=1):

  if data:
    pcap_data, stats = data
    # get only the first item
    pcap_file = next(iter(pcap_data))
    pcap_data = pcap_data[pcap_file]

  sns.set_palette(sns.color_palette("hls", 20))
  colors = {}
  ax.set_title('Overview of Network Traffic')
  box = ax.get_position()
  ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
  abs_start = .0
  for ind, conv in enumerate(pcap_data['convs']):
    start = conv['values'][0][0]
    if abs_start == 0:
      abs_start = start
    start = start - abs_start
    end = conv['values'][-1][0]
    if len(conv['values']) > 4:
      end = conv['values'][-4][0]
    end = end - abs_start

    ip = conv['id'][1]
    label = None
    if ip not in colors:
      colors[ip] = next(ax._get_lines.prop_cycler)['color']
      label = ip
    color = colors[ip]
    ax.scatter(start, ind, color=color)
    ax.scatter(end, ind, color=color)
    ax.plot((start, end), (ind, ind), linewidth=4, label=label, color=color)
    #ax.text(start, ind, ip, fontsize=8)

  ax.set_xlabel('Timeline in seconds')
  ax.set_ylabel('Conversations')
  #ax.legend(loc='lower center', bbox_to_anchor=(1, 0.5), ncol=ncol)
  #ax.legend()


def plotIPSiteFreq(fig, data):
  pcap_data, stats = data

#  ipsite_freq = {}
#  for ip, flist in stats['ip_freq'].items():
#    ipsite_freq.setdefault(ip, set())
#    for fname in flist:
#      ipsite_freq[ip].add(pcap_data[fname]['site'])
#
#  #reverse the dictionary
#
#  siteip_freq = {}
#  for ip, sites in ipsite_freq.items():
#    n=len(sites)
#    tag = '-'.join(sites)
#    siteip_freq.setdefault(n, {})
#    siteip_freq[n].setdefault(tag, [])
#    siteip_freq[n][tag].append(ip)
#
#  gs = fig.add_gridspec(3,2)
#  ax0 = fig.add_subplot(gs[0,0])
#  ax0.bar(siteip_freq[1].keys(),[len(i) for i in siteip_freq[1].values()], align='center')
#
#  ax1 = fig.add_subplot(gs[0,1])
#  ax1.bar(siteip_freq[2].keys(),[len(i) for i in siteip_freq[2].values()], align='center')
#
#  ax2 = fig.add_subplot(gs[1,0])
#  ax2.bar(siteip_freq[3].keys(),[len(i) for i in siteip_freq[3].values()], align='center')

  ipsite_freq = {ip:len(set([pcap_data[fname]['site'] for fname in flist])) for ip, flist in stats['ip_freq'].items()}

  ax = fig.gca()
  v = Counter(ipsite_freq.values())
  ax.bar(v.keys(), v.values(), align='center')
  #x = ax.hist(ipsite_freq.values())
  #ax.set_xticks(list(v.keys()))
  ax.set_title('Histogram of IP address occurence on multiple sites')
  ax.set_xlabel('Site Count')
  ax.set_ylabel('IP Count')



def plotIPFreq(fig, data):
  pcap_data, stats = data
  ip_freq = Counter([len(set(flist)) for ip, flist in stats['ip_freq'].items()])

  ax0 = fig.add_subplot(2, 1, 1)
  ax0.bar(ip_freq.keys(), ip_freq.values(), align='center')
  ax0.set_title('Histogram of IP address occurence on multiple pcaps')
  ax0.set_xlabel('Pcap Count')
  ax0.set_ylabel('IP Count')

  ax1 = fig.add_subplot(2, 1, 2)
  ax1.bar(ip_freq.keys(), ip_freq.values(), align='center')
  ax1.set_title('Histogram of IP address occurence on multiple pcaps')
  ax1.set_xlabel('Pcap Count')
  ax1.set_xscale('log')
  ax1.set_ylabel('IP Count')
  ax1.set_yscale('log')


def plotSiteHistogram(fig, data):
  pcap_data, stats = data

  ax = fig.gca()
  sites = Counter([v['site'] for k,v in pcap_data.items()])
  site_lookup = {
      'utc.edu':'Chattanooga, TN',
      'cenic.net':'Los Angeles, CA',
      'wisc.edu':'Madison, WI',
      'hawaii.edu':'Honolulu, HI',
      'nyu.edu':'New York, NY'
      }
  res = {}
  for k,v in sites.items():
    for site,city in site_lookup.items():
      if site in k:
        res[city] = v

  ax.bar(res.keys(), res.values(), align='center')
  ax.set_title('Histogram of Sites')
  ax.set_xlabel('Sites')
  ax.set_ylabel('Pcap Count')



def plotUniqueIPvsSite(fig, data):
  pcap_data, stats = data
  res = {ip:(len(set(flist)),len(set([pcap_data[fname]['site'] for fname in flist]))) for ip,flist in stats['ip_freq'].items()}
  plotdata = {}
  for k,v in res.items():
    plotdata.setdefault(v,0)
    plotdata[v]+=1

  ax = fig.gca()
  for k,v in plotdata.items():
    ax.scatter(k[0],k[1],s=v*2)
  ax.set_title('Unique IP distribution over Pcap and Site')
  ax.set_xlabel('Pcap Count')
  ax.set_ylabel('Site Count')


def plotDuplicateType(fig, data):
  pcap_data, stats = data
  res = []
  maxInd = 0
  for fname,data in pcap_data.items():
    for ind,conv in enumerate(data['processed']):
      res.append((ind,conv[1]))
      maxInd = max(maxInd, ind)

  maxInd+=1

  ax0 = fig.add_subplot(2,1,1)
  ax1 = fig.add_subplot(2,1,2)
  res2 = {}
  for (ind,dupType),count in Counter(res).items():
    res2.setdefault(dupType,[0]*maxInd)
    res2[dupType][ind] = count

  for dupType,values in res2.items():
    if max(values) > 100:
      ax1.plot(values, label=str(dupType))
    else:
      ax0.plot(values, label=str(dupType))

  ax0.set_title('Distribution of Duplicates inside Pcap')
  ax0.set_xlabel('Conversation Index')
  ax0.set_ylabel('Instance Count')
  ax0.legend()
  ax1.legend()



def plotConvTotalSize1(fig, data):
  pcap_data, stats = data

  res = []
  test = []
  for fname,data in pcap_data.items():
    for ind, conv in enumerate(data['convs']):
      totalSize = 0
      for pkt in conv['values']:
        totalSize+=pkt[1]
      res.append((ind,totalSize))
      if ind == 0:
        test.append(totalSize)

  ax = fig.gca()

  test.sort()
  ax.plot(test)
  ax.set_title('Conversation Size for 1st Conversation')
  ax.set_xlabel('Instance Number')
  ax.set_ylabel('Conversation size (total bytes)')


def plotConvTotalSize2(fig, data):
  pcap_data, stats = data

  res = []
  test = []
  for fname,data in pcap_data.items():
    for ind, conv in enumerate(data['convs']):
      totalSize = 0
      for pkt in conv['values']:
        totalSize+=pkt[1]
      res.append((ind,totalSize))
      if ind == 0:
        test.append(totalSize)

  ax = fig.gca()

  n_bins = 50
  # plot the cumulative histogram
  n, bins, patches = ax.hist(test, n_bins, density=True, histtype='step',
                                 cumulative=True, label='Empirical')

  ax.hist(test, bins=bins, density=True, histtype='step', cumulative=-1,
              label='Reversed emp.')

  #x = ax.hist(test, bins=bins, histtype='stepfilled', alpha=0.3, density=True)
  #ax.set_xticks(x[1])
  ax.set_title('Conversation Size for 1st Conversation')
  ax.set_xlabel('Conversation size (bytes)')
  ax.set_ylabel('Likelihood of Occurence')


def plotConvTotalSize3(fig, data):
  pcap_data, stats = data

  x = []
  y = []
  for fname,data in pcap_data.items():
    for ind, conv in enumerate(data['convs']):
      totalSize = 0
      for pkt in conv['values']:
        totalSize+=pkt[1]

      if totalSize!= 0:
        x.append(ind)
        y.append(totalSize)

  ax = fig.gca()

  ax.scatter(x,y)


def plot(data):

  plotFuncs = {
      #'convCount': plotConvCount,
      #'concurrent': plotConcurrent,
      ##'timeline': pcapTimeline,
      #'ipfreq': plotIPFreq,
      #'ipsitefreq': plotIPSiteFreq,
      #'sites': plotSiteHistogram,
      #'uniqueip-site': plotUniqueIPvsSite,
      #'duplicate-type': plotDuplicateType,
      #'totalsize1': plotConvTotalSize1,
      #'totalsize2': plotConvTotalSize2,
      #'totalsize3': plotConvTotalSize3,
      #'convLine1': plotConvLine1,
      #'convLine2': plotConvLine2,
      ##'cdf-time': plotCDFTime,
      ##'cdf-len': plotCDFLen,
      #'cdf-conv': plotCDFConvs,
      'fit-time': plotCDFTimeFit,
      'fit-15k': plotCDF15kTimeFit,
      'fit-len': plotCDFLenFit,
      #'cdf-bw': plotCDFBandwidth,
      #'res-table': genResultTable,
      #'weekly': plotWeeklyData
  }

  figCounter=itertools.count()
  for k,v in plotFuncs.items():
    fig = plt.figure()
    v(fig, data)
    fig.savefig(f"{PLOTFOLDER}/{k}.png", bbox_inches="tight")


def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--file", "-f", dest="flist", action='append', required=True)
  parser.add_argument("--interactive", "-i", dest="interactive", action='store_true')
  return parser.parse_args()



def detect_outliers(inp, get_value, reason):

  arr = [get_value(v) for k,v in inp.items()]
  m = np.mean(arr)
  s = np.std(arr)

  for fname, values  in inp.items():
    val = get_value(values)
    if filter_outliers_z_score(val, m, s):
      values.setdefault('outlier',[]).append(reason)

  return inp


if __name__ == '__main__':
  OPTS = parse_args()
  FILES = {}
  DATA = {}
  OWD = os.getcwd()

  for flist in OPTS.flist:
    tag = flist[:-4]
    FILES[tag] = [f for f in readList(flist)]
    os.chdir(tag)

    print(f"Working on {tag}")


    def filterLower(x):
      try:
        ts = x['convs'][0]['values'][0][0]
        return datetime(2019,8,27).timestamp() < ts and ts < datetime(2019,9,3).timestamp()
      except:
        return False

    kwargs={
        "groupBy": process.site_groupBy,
        "preprocess": process.concurrent_preprocess,
        "updateFuncs": process.UPDATES
       # "filterBy": filterLower
    }

    DATA[tag] = process.process(FILES[tag], **kwargs)

    PLOTFOLDER = os.path.expanduser('~/Desktop/%s' %(tag))
    try:
      os.makedirs(PLOTFOLDER)
    except OSError as e:
      if e.errno != errno.EEXIST:
        raise

    print(f"Outliers..")
    detect_outliers(DATA[tag][0],lambda x: x['dur'], 'dur')
    detect_outliers(DATA[tag][0],lambda x: x['len'], 'len')
    for site in SITE_LOOKUP:
      tmp_data = {k:v for k,v in DATA[tag][0].items() if v['site'] == site}
      detect_outliers(tmp_data,lambda x: x['dur'], 'site_dur')
      detect_outliers(tmp_data,lambda x: x['len'], 'site_len')


    print(f"Plots will be saved under {PLOTFOLDER}")

    plot(DATA[tag])
    #Revert back to Original Working Directory
    os.chdir(OWD)

  PLOTFOLDER = os.path.expanduser('~/Desktop/%s' %("-".join(DATA.keys())))
  try:
    os.makedirs(PLOTFOLDER)
  except OSError as e:
    if e.errno != errno.EEXIST:
      raise


  if OPTS.interactive:
    import IPython
    IPython.embed(using=False)

