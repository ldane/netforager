#!/usr/bin/env python
# coding: utf-8
import argparse
import codecs
import dpkt
import pcapy
import struct

from collections import OrderedDict
from dpkt.compat import compat_ord
from utils import *

def unpack(self, buf):
  dpkt.Packet.unpack(self, buf)
  off = self.__hdr_len__
  cnt = self.qd  # FIXME: This relies on this being properly set somewhere else
  self.qd = []
  for _ in range(cnt):
    q, off = self.unpack_q(buf, off)
    self.qd.append(q)
  for x in ('an', 'ns', 'ar'):
    cnt = getattr(self, x, 0)
    setattr(self, x, [])
    for _ in range(cnt):
      try:
        rr, off = self.unpack_rr(buf, off)
        getattr(self, x).append(rr)
      except:
        break
  self.data = b''

def main(fname):
  # globals
  dnsQueries = OrderedDict()

  with open(fname) as fhandler:
    for f in fhandler:
      f = f.strip()

      for (hdr,buf) in pcapy_iter(f,'proto \udp and port 53'):
        eth = dpkt.ethernet.Ethernet(buf)
        ip = eth.data
        udp = ip.data
        #Monkey Patch
        dpkt.dns.DNS.unpack = unpack
        try:
          dns = dpkt.dns.DNS(udp.data)
          qdname = dns.qd[0].name
          dnsQueries.setdefault(qdname,[]).append((hdr,eth,dns))
        except:
          import IPython; IPython.embed()


def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--file","-f", dest="fname", type=str, required=True)

  return parser.parse_args()

if __name__ == '__main__':
  opts = parse_args()
  main(opts.fname)

