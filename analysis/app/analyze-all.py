#!/usr/bin/env python
# coding: utf-8
import argparse
import itertools
import os, errno

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import numpy as np
import seaborn as sns

from collections import Counter
from datetime import datetime
from tabulate import tabulate

import process
from utils import *


def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--file", "-f", dest="flist", action='append', required=True)
  parser.add_argument("--interactive", "-i", dest="interactive", action='store_true')
  return parser.parse_args()



if __name__ == '__main__':
  OPTS = parse_args()
  FILES = {}
  #DATA = {}
  UPDATES = {
      'sni_ip': process.updateSNI,
      'entry_freq': process.updateEntryFreq,
      'ip_freq': process.updateIPFreq
      }

  OWD = os.getcwd()

  ipAppFreq = {}
  convsLen = []

else:

  for flist in OPTS.flist:
    tag = flist[:-4]
    FILES[tag] = [f for f in readList(flist)]
    os.chdir(tag)

    print(f"Working on {tag}")


    #DATA[tag] = process.process(FILES[tag], groupBy=process.site_groupBy, preprocess=process.concurrent_preprocess, updateFuncs=UPDATES)
    D = process.process(FILES[tag], groupBy=process.site_groupBy, preprocess=process.concurrent_preprocess, updateFuncs=UPDATES)

    #Revert back to Original Working Directory
    os.chdir(OWD)

    pcap_data, stats = D
    for ip in stats['ip_freq']:
      ipAppFreq.setdefault(ip, set())
      ipAppFreq[ip].add(tag)

    for fname in pcap_data:
      convsLen.append((tag,len(pcap_data[fname]['convs'])))



  #PLOTFOLDER = os.path.expanduser('~/Desktop/%s' %("-".join(DATA.keys())))
  #try:
  #  os.makedirs(PLOTFOLDER)
  #except OSError as e:
  #  if e.errno != errno.EEXIST:
  #    raise

  tagIPFreq = {}
  for ip, tagSet in ipAppFreq.items():
    tags = '-'.join(tagSet)
    tagIPFreq.setdefault(tags,[])
    tagIPFreq[tags].append(ip)

  #plotData = {}
  #for tag,ipset in tagIPFreq.items():
  #  n = len(tag.split('-'))
  #  plotData.setdefault(n,{})
  #  plotData[n][tag] = len(ipset)

  #for n,tags in plotData.items():
  #  fig = plt.figure()
  #  ax = fig.gca()
  #  ax.bar(tags.keys(), tags.values(), align='center')
  #  plt.xticks(rotation=30)
  #  ax.set_xlabel('Applications')
  #  ax.set_ylabel('Unique IP Count')
  #  ax.set_title('Application vs Unique IP Distribution')
  #  fig.savefig('%s/%d.png' %(PLOTFOLDER, n))

  if OPTS.interactive:
    import IPython
    IPython.embed(using=False)

