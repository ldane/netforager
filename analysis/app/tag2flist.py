#!/usr/bin/env python

import argparse
import itertools
import json
import os

from pathlib import Path
from urllib.parse import urlparse


def urls2tag(urls):
  return '|'.join(urls)

def tagList(url):
  #maybe use atexit to register
  if not hasattr(tagList, 'tags'):
    import atexit

    def savecounter():
      with open("tags.lst", "w") as outfile:
        print(tagList.tags)
        json.dump(tagList.tags, outfile)

    atexit.register(savecounter)

  print(url)
  if url not in tagList.tags:
    print('Adding')
    tagList.tags[url] = next(tagList.cnt)
  return tagList.tags[url]


def url2flist(path):
  res = {}
  tags = {}
  cnt = itertools.count()

  pcaps = Path(path).glob("**/client*/clientResults/target-urls.json")
  for fname in pcaps:
    with open(fname) as f:
      urls = json.load(f)
      tag = urls2tag(urls)
      if tag not in tags:
        tagID = next(cnt)
        tags[tag] = tagID
        res[tagID] = []

      tagID = tags[tag]
      file_tag = '-'.join([urlparse(url).netloc for url in urls])
      pcap_file = '%scapture-%s.pcap' %(str(fname)[:-len('target-urls.json')],file_tag)
      if not os.path.isfile(pcap_file):
        #'Pcap file not found'
        continue
      res[tagID].append(pcap_file)

  with open('tags.lst', 'w') as outfile:
    json.dump(tags, outfile)

  return res

def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--directory", "-d", dest="path", type=str, default='./')
  return parser.parse_args()


if __name__ == '__main__':
  OPTS = parse_args()

  RES = url2flist(OPTS.path)

  for key, value in RES.items():
    with open('%s.lst' % (key), 'w') as fhandler:
      for i in value:
        fhandler.write("%s\n" % i)
