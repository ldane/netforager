import json
import numpy as np
import pcapy
import socket
import sys


from collections import namedtuple
from concurrent.futures import ProcessPoolExecutor, as_completed
from numpy import abs as np_abs
from scipy.stats import linregress, norm
import scipy.optimize
from tqdm import tqdm



def decodeTCPFlags(tcpflags):
  #from dpkt.tcp import TH_ACK, TH_CWR, TH_ECE, TH_FIN, TH_PUSH, TH_RST, TH_SYN, TH_URG
  TCPFlags = namedtuple('TCPFlags', 'fin syn rst psh ack urg ece cwr'.split())
  return TCPFlags(
   ( tcpflags & 2**0 ) != 0,
   ( tcpflags & 2**1 ) != 0,
   ( tcpflags & 2**2 ) != 0,
   ( tcpflags & 2**3 ) != 0,
   ( tcpflags & 2**4 ) != 0,
   ( tcpflags & 2**5 ) != 0,
   ( tcpflags & 2**6 ) != 0,
   ( tcpflags & 2**7 ) != 0)


def inet_to_str(inet):
  """Convert inet object to a string
      Args:
          inet (inet struct): inet network address
      Returns:
          str: Printable/readable IP address
  """
  # First try ipv4 and then ipv6
  try:
    return socket.inet_ntop(socket.AF_INET, inet)
  except ValueError:
    return socket.inet_ntop(socket.AF_INET6, inet)

def readList(fname):
  with open(fname) as f:
    for line in f:
      yield line.strip()

def pcapy_iter(filename, filterstr=None):
  try:
    pcapHandler = pcapy.open_offline(filename)
  except:
    print(f'Wrong file header: {filename}')
    return
  if filterstr:
    bpfProg = pcapy.compile(pcapy.DLT_EN10MB, 60000, filterstr, True, 0)
  header,buf = pcapHandler.next()
  while header:
    if filterstr:
      if bpfProg.filter(buf):
        yield (header,buf)
    else:
      yield (header,buf)
    try:
      header,buf = pcapHandler.next()
    except:
      #print 'Truncated: ',filename
      pass

# Parallel process
def parallel_process(array, function, n_jobs=6, use_kwargs=False, front_num=3):
  """
    A parallel version of the map function with a progress bar. 

    Args:
      array (array-like): An array to iterate over.
      function (function): A python function to apply to the elements of array
      n_jobs (int, default=16): The number of cores to use
      use_kwargs (boolean, default=False): Whether to consider the elements of array as dictionaries of 
        keyword arguments to function 
      front_num (int, default=3): The number of iterations to run serially before kicking off the parallel job. 
        Useful for catching bugs
    Returns:
      [function(array[0]), function(array[1]), ...]
  """
  #We run the first few iterations serially to catch bugs
  if front_num > 0:
    front = [function(**a) if use_kwargs else function(a) for a in array[:front_num]]
  #If we set n_jobs to 1, just run a list comprehension. This is useful for benchmarking and debugging.
  if n_jobs==1:
    return front + [function(**a) if use_kwargs else function(a) for a in tqdm(array[front_num:])]
  #Assemble the workers
  with ProcessPoolExecutor(max_workers=n_jobs) as pool:
    #Pass the elements of array into function
    if use_kwargs:
      futures = [pool.submit(function, **a) for a in array[front_num:]]
    else:
      futures = [pool.submit(function, a) for a in array[front_num:]]
    kwargs = {
      'total': len(futures),
      'unit': 'it',
      'unit_scale': True,
      'leave': True,
    }
    #Print out the progress as tasks complete
    for f in tqdm(as_completed(futures), **kwargs):
      pass
  out = []
  #Get the results from the futures. 
  for i, future in tqdm(enumerate(futures)):
    try:
      out.append(future.result())
    except Exception as e:
      out.append(e)

  return front + out


def printWithHeader(data, header=None):
  if header:
    print("{:=^60}".format(header))
  print(data, "\n\n\n")

def convFile(fname):
  fname = fname.split('/')
  fname = '%s-%s-conv.json' %(fname[-4],fname[-1][:-5])
  return fname

def loadPcapData(flist):
  pcap_data = {}

  for pcap_file in flist:
    with open(convFile(pcap_file)) as f:
      pcap_data[pcap_file] = json.load(f)
      if pcap_data[pcap_file] is None:
        print(f'empty pcap? {pcap_file}')
        del(pcap_data[pcap_file])

  return pcap_data


def format_bytes(num, pos=None):
  suffix = 'B'
  for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
    if abs(num) < 1024.0:
      return "%3.2f%s%s" % (num, unit, suffix)
    num /= 1024.0
  return "%.2f%s%s" % (num, 'Yi', suffix)
  # 2**10 = 1024
  #2power = 2**10
  #n = 0
  #power_labels = {0 : '', 1: 'K', 2: 'M', 3: 'G', 4: 'T', 5: 'P', 6: 'E', 7: 'Z', 8: 'Y'}
  #while size > power:
  #  size /= power
  #  n += 1
  #return str(round(size))+' '+power_labels[n]+'B'


def filter_outliers_z_score(y, mean, stdev, threshold = 2.5):
  z_score = (y - mean) / stdev
  return np_abs(z_score) > threshold

def reject_outliers(data, thr=2.5):
      return data[abs(data - np.mean(data)) < thr * np.std(data)]

def linFit(arr, detectOutlier=False, thr=2.5):
  x = np.sort(arr)
  if detectOutlier:
    x = reject_outliers(x, thr)

  y = np.linspace(0, 1, len(x), endpoint=False)
  lin = linregress(x, y)

  x1 = -lin.intercept/lin.slope
  x2= (1-lin.intercept)/lin.slope

  return (x, y, lin, [x1,x2])

def curve_fit(data):
  x = np.sort(data)
  x = reject_outliers(x)
  y = np.linspace(0, 1, len(x))
  f = lambda x,mu,sigma: norm(mu,sigma).cdf(x)

  #def f(x, k, x0):
  #  return 1 / (1. + np.exp(-k * (x - x0)))

  p0 = [np.std(x), np.mean(x)]
  popt, pcov = scipy.optimize.curve_fit(f,x,y, p0)

  xP = np.linspace(norm(*popt).ppf(0.25),norm(*popt).ppf(0.75), 100)
  lin = linregress(xP, f(xP, *popt))

  return (x, y, popt, pcov, lin, f)
