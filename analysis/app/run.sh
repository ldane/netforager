#!/usr/bin/env bash
DATA="$HOME/data/pcaps"
SRC="$HOME/src/srvc-id"

function steps {
	TARGET=$1
	mkdir -p $1
	cd $1
	python $SRC/extractip.py -f ../$TARGET.lst
	python $SRC/fillmeta.py -f ../$TARGET.lst
	python $SRC/addsni.py -f ../$TARGET.lst
	cd ..
}

mkdir -p run
cd run

python $SRC/tag2flist.py -d $DATA

for i in {0..14}; do
	steps $i
done

