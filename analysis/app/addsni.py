#!/usr/bin/env python
# coding: utf-8
import argparse
import glob
import json

from utils import *
from sni import sniFromPacket

from extractip import convFile

def addsni(pFile):
  cFile = convFile(pFile)

  data = None
  with open(cFile) as fhandler:
    data = json.load(fhandler)
#Check only 443
  for (hdr,buf) in pcapy_iter(pFile,'tcp and port 443'):
    sni = None
    try:
      sni = sniFromPacket(buf)
    except:
      pass

    if sni is not None:
      ts = hdr.getts()
      ts = ts[0]+0.000001*ts[1]

      for convs in data['convs']:
        for v in convs['values']:
          if v[0] == ts:
            convs.setdefault('sni',[])
            convs['sni'].append(str(sni))

  with open(cFile,'w') as fw:
    json.dump(data,fw)

def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--file","-f", dest="flist", type=str, required=True)
  return parser.parse_args()

if __name__ == '__main__':
  opts = parse_args()
  files = [f for f in readList(opts.flist)]
  parallel_process(files, addsni)

