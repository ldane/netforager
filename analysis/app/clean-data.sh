#!/usr/bin/env sh

rm -rf run
cd ~/data

find ~/data -name '*-fix.pcap' -exec rm {} \;

cd -
