#!/usr/bin/env python
# coding: utf-8
import argparse
import json

import networkx as nx
import matplotlib.pyplot as plt

from utils import *
from pattern import concurrent_preprocess


def conversationList(pcap_data):
  convs = pcap_data['convs']
  return [(c['id'][1], c.get('sni', None)) for c in convs]


def calculate_freq(flist):
  freq = {'node':{}, 'edge':{}, 'chain':{}}
  pcap_data = loadPcapData(flist[:50])

  # Calculate
  for fname, fdata in pcap_data.items():
    # Extract only destination ips
    convs = concurrent_preprocess([conv['id'][1] for conv in fdata['convs']])
    pcap_data[fname]['preproc'] = convs

    for i, cur in enumerate(convs):
      freq['node'].setdefault(i, {}).setdefault(cur, 0.)
      freq['node'][i][cur] += 1

    for i, (cur, cur_next) in enumerate(zip(convs, convs[1:])):
      freq['edge'].setdefault(i, {}).setdefault((cur, cur_next), 0.)
      freq['edge'][i][(cur, cur_next)] += 1

      freq['chain'].setdefault((cur, cur_next), 0.)
      freq['chain'][(cur, cur_next)] += 1

  # Normalize
  for level in freq['node']:
    total = sum(freq['node'][level].values())
    print(f"Level: {level}, Total: {total}")
    for item in freq['node'][level]:
      freq['node'][level][item] = freq['node'][level][item]/total

  total = {}
  for level in freq['edge']:
    # Calculate total for item
    for item in freq['node'][level]:
      total[item] = sum([v for t,v in freq['edge'][level].items() if t[0] == item])

  ups_count = 0
  for level in freq['edge']:
    for t in freq['edge'][level]:
      if total[t[0]] == 0:
        print (f'Ups {level} = {t}')
        ups_count+=1
        freq['edge'][level][t] = 0
        continue
      freq['edge'][level][t] = freq['edge'][level][t]/total[t[0]]
  print (f'Total Ups: {ups_count}')

  G = nx.Graph()

  for level in freq['node']:
    for item in freq['node'][level]:
      #if freq['node'][level][item] > 0.5:
      G.add_node('%s_%d' %(item, level), order=level)

  for level in freq['edge']:
    for item in freq['edge'][level]:
      #if freq['edge'][level][item] > 0.5:
      x = '%s_%d' %(item[0], level)
      y = '%s_%d' %(item[1], level+1)
      G.add_edge(x, y, weight = freq['edge'][level][item])

  plt.figure(figsize=(100,100))
  nx.draw(G, with_labels=True, font_weight='bold')
  plt.savefig('/home/limon/Desktop/test.png')

  import IPython
  IPython.embed()


def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--file", "-f", dest="flist", type=str, required=True)
  return parser.parse_args()


if __name__ == '__main__':
  OPTS = parse_args()
  FILES = [f for f in readList(OPTS.flist)]
  calculate_freq(FILES)
