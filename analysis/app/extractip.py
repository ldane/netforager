#!/usr/bin/env python2

import argparse
import json
import os

import dpkt
import pcapy

from collections import OrderedDict

from utils import *

def parsePcap(fname):
  pktlist = OrderedDict()

  for (header,buf) in pcapy_iter(fname):
    timeStamp = header.getts()
    timeStamp = timeStamp[0]+0.000001*timeStamp[1]

    eth = dpkt.ethernet.Ethernet(buf)
    if not isinstance(eth.data, dpkt.ip.IP):
      #print 'Not IP'
      continue
    ip = eth.data

    if not isinstance(ip.data, dpkt.tcp.TCP):
      #print 'Not TCP'
      continue

    tcp = ip.data

    src = inet_to_str(ip.src)
    dst = inet_to_str(ip.dst)
    sport = tcp.sport
    dport = tcp.dport

    if '172.17.0.100' in dst:
      dst, src = src, dst
      dport, sport = sport, dport

    tcptuple = (src, dst, sport, dport)
    tcpflags = decodeTCPFlags(tcp.flags)

    if tcptuple not in pktlist:
      pktlist[tcptuple] = []

    #Calculate tcp payload size
    # IP Length - (IP Header Length + TCP Offset) * 4
    payload_len = ip.len - (ip.hl + tcp.off) * 4
    pktlist[tcptuple].append((timeStamp, payload_len, tcpflags))

  return pktlist


def extract(fname):
  pktlist = parsePcap(fname)

  #Split into Conversations
  result = []
  for p,flags in pktlist.items():
    result.append({'id':p,'values':[]})
    finFound=False

    for (ts, length, flag) in flags:
      if flag.fin:
        finFound=True
      elif finFound and flag.syn:
        result.append({'id':p,'values':[]})
        finFound=False
      result[-1]['values'].append((ts,length))

  out = convFile(fname)

  if os.path.isfile(out):
    print(out, fname)
    print('File exists')
    return

  with open(out, 'w') as j:
    json.dump(result,j)


def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--file","-f", dest="flist", type=str, required=True)
  return parser.parse_args()

if __name__ == '__main__':
  opts = parse_args()
  files = [f for f in readList(opts.flist)]
  parallel_process(files, extract)
