#!/usr/bin/env python
# coding: utf-8
import argparse
import json
import os

from datetime import datetime

from utils import *

def simple_filterBy(data):
  return True


def simple_groupBy(data):
  return hash(data)


def simple_preprocess(data):
  return data


def site_groupBy(data):
  return data['site']


def weekday_groupBy(data):
  ts = data['capture']
  ts = datetime.fromtimestamp(ts)
  return ts.weekday()


def hour_groupBy(data):
  ts = data['capture']
  ts = datetime.fromtimestamp(ts)
  return ts.hour


def indepth_groupBy(data):
  ts = data['capture']
  ts = datetime.fromtimestamp(ts)
  return (data['site'], ts.weekday(), ts.hour)


def concurrent_preprocess(data):
  new_data = []
  ind = 0

  while ind < len(data):
    if ind + 1 < len(data) and data[ind] == data[ind+1]:
      next_ind = ind + 1
      while next_ind < len(data) and data[ind] == data[next_ind]:
        next_ind += 1
      new_data.append((data[ind], next_ind-ind))
      ind = next_ind
    else:
      new_data.append((data[ind], 1))
      ind += 1

  return new_data




def loadPcaps(flist, filterBy):
  pcaps = {}
  for pcap_file in flist:
    cur_data = None
    conv_file = convFile(pcap_file)

    with open(conv_file) as f:
      cur_data = json.load(f)

      if cur_data is None:
        print('empty pcap?')
        continue

    if not filterBy(cur_data):
      continue

    cur_data['convs'] = sorted(cur_data['convs'], key=lambda x: x['values'][0])

    pcaps[pcap_file] = cur_data

  return pcaps

def updateSNI(sni_ip, data):
  for conv in data['convs']:
    if 'sni' not in conv:
      continue
    for sni in conv['sni']:
      sni_ip.setdefault(sni, [])
      sni_ip[sni].append(conv['id'][1])

def updateEntryFreq(entry_freq, data):
  key = data['key']
  entry = data['convs'][0]['id'][1]
  entry_freq.setdefault(key, {})
  entry_freq[key].setdefault(entry, [])
  entry_freq[key][entry].append(data['fname'])

def updateIPFreq(ip_freq, data):
  for conv in data['convs']:
    ip = conv['id'][1]
    ip_freq.setdefault(ip, [])
    ip_freq[ip].append(data['fname'])

def updateSiteDur(site_dur, data):
  site_dur.setdefault(data['site'], []).append(data['dur'])

def updateSiteLen(site_len, data):
  site_len.setdefault(data['site'], []).append(data['len'])

def process(flist, preprocess=simple_preprocess, groupBy=simple_groupBy, filterBy=simple_filterBy, updateFuncs={}):

  pcap_data = loadPcaps(flist, filterBy)

  stats = {}
  for key in updateFuncs:
    stats[key] = {}

  for pcap_file in flist:
    if pcap_file not in pcap_data:
      continue
    cur_data = pcap_data[pcap_file]
    cur_data['fname'] = pcap_file
    if len(cur_data['convs']) == 0:
      print('No convs in %s' %(pcap_file))
      del(pcap_data[pcap_file])
      continue

    # Get the current key
    cur_data['key'] = groupBy(cur_data)

    # process data by provided process function
    cur_data['processed'] = preprocess([x['id'][1] for x in cur_data['convs']])

    # Calculate total conversation lengths
    for conv in cur_data['convs']:
      conv['len'] = sum([pkt[1] for pkt in conv['values']])
      conv['cnt'] = len(conv['values'])
    cur_data['len'] = sum([conv['len'] for conv in cur_data['convs']])

    if cur_data['len'] == 0:
      print('No data retrieved in %s' %(pcap_file))
      del(pcap_data[pcap_file])
      continue

    # Calculate total duration
    cur_data['start'] = cur_data['convs'][0]['values'][0][0]
    maxT = 0.
    convs_t = [cp for c in cur_data['convs'] for cp in c['values']]

    lastT = cur_data['start']
    for v in sorted(convs_t, key=lambda x: x[0]):
      if v[0] - lastT > 2.:
        #print('2 second passed stop')
        break
      if v[1] != 0 and v[0] > maxT:
        maxT = v[0]
      lastT = v[0]

    if maxT == 0:
      print('No data retrieved in %s' %(pcap_file))
      del(pcap_data[pcap_file])
      continue
      #maxT = cur_data['convs'][-1]['values'][-1][0]
    cur_data['end'] = maxT
    cur_data['dur'] = cur_data['end'] - cur_data['start']

    for key,func in updateFuncs.items():
      func(stats[key], cur_data)

  return (pcap_data, stats)



def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--file", "-f", dest="flist", type=str, required=True)
  parser.add_argument("--interactive", "-i", dest="interactive", action='store_true')
  return parser.parse_args()



UPDATES = {
    'sni_ip': updateSNI,
    'entry_freq': updateEntryFreq,
    'ip_freq': updateIPFreq,
    'site_dur': updateSiteDur,
    'site_len': updateSiteLen
    }

if __name__ == '__main__':
  OPTS = parse_args()
  FILES = [f for f in readList(OPTS.flist)]
  tag = '.'.join(OPTS.flist[4:].split('.')[:-1])
  print(f"Working on {tag}")
  DESKTOP = os.path.expanduser('~/Desktop/%s' %(tag))

  DATA = process(FILES, groupBy=site_groupBy, preprocess=concurrent_preprocess, updateFuncs=UPDATES)

  if OPTS.interactive:
    import IPython
    IPython.embed(using=False)

