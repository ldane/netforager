#!/usr/bin/env python
# coding: utf-8
import argparse
import glob
import json
import logging
import os

import geni.rspec.vtsmanifest as VTSM
from extractip import convFile
from utils import readList


def getSite(fname):
  pattern = '/'.join(fname.split('/')[:-3]+['*', 'manifest.xml'])
  flist = glob.glob(pattern)
  if len(flist) == 0:
    print(f'Cant find manifest {fname}')
    return
  manifest_file = flist[0]
  capture_ts = os.path.getmtime(manifest_file)
  manifest = VTSM.Manifest(manifest_file)
  conv_file = convFile(fname)
  data = None
  with open(conv_file) as f:
    try:
      convs = json.load(f)
    except:
      print(f"Error on file: {conv_file}")
      return
  if isinstance(convs, list):
    data = {
        'convs': convs,
        'site': manifest.host,
        'capture': capture_ts
    }
  elif isinstance(convs, dict):
    data = convs
    data['site'] = manifest.host
    data['capture'] = capture_ts

  if data is None:
    print('json file might be corrupt')
    return

  with open(conv_file, 'w') as f:
    json.dump(data, f)


def doGetSite(flist):
  for fname in flist:
    getSite(fname)


def parse_args():
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--file", "-f", dest="flist", type=str, required=True)
  return parser.parse_args()


if __name__ == '__main__':
  OPTS = parse_args()
  FILES = [f for f in readList(OPTS.flist)]
  doGetSite(FILES)
