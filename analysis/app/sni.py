import dpkt
import struct

from dpkt.ssl import parse_variable_array


def parse_extensions(buf):
  """
  Parse TLS extensions in passed buf. Returns an ordered list of extension tuples with
  ordinal extension type as first value and extension data as second value.
  Passed buf must start with the 2-byte extensions length TLV.
  http://www.iana.org/assignments/tls-extensiontype-values/tls-extensiontype-values.xhtml
  """
  extensions_length = struct.unpack('!H', buf[:2])[0]
  extensions_length = min(extensions_length, len(buf))
  extensions = []

  pointer = 2
  while pointer < extensions_length:
    if len(buf) < pointer+2:
      break
    ext_type = struct.unpack('!H', buf[pointer:pointer+2])[0]
    pointer += 2
    if len(buf) < pointer+2:
      break
    ext_data, parsed = parse_variable_array(buf[pointer:], 2)
    extensions.append((ext_type, ext_data))
    pointer += parsed
  return extensions

def sniFromExtension(extension):
  if len(extension) == 0:
    return
  import struct
  n=len(extension) - struct.calcsize('!hbh')
  return struct.unpack('!hbh%ds' %(n),extension)[3]

def sniFromPacket(buf):
  eth = dpkt.ethernet.Ethernet(buf)
  # Is it an IP Packet?
  if not isinstance(eth.data, dpkt.ip.IP):
    return
  ip = eth.data
  # Is it a TCP Packet?
  if not isinstance(ip.data, dpkt.tcp.TCP):
    return
  tcp = ip.data

  if len(tcp.data) == 0:
    return

  #Handshake?
  if tcp.data[0] != 22:
    return

  #TLS?
  if tcp.data[1] != 3:
    return

  #2 TLS1.?
  #3,4 length
  #5 ClientHello
  if tcp.data[5] != 1:
    return

  #6,7,8 length
  #Monkey Patch dpkt to handle incomplete TLSCLientHello
  dpkt.ssl.parse_extensions = parse_extensions
  clientHello = dpkt.ssl.TLSClientHello(tcp.data[9:])

  for ext in clientHello.extensions:
    if ext[0] == 0:
      return sniFromExtension(ext[1])

