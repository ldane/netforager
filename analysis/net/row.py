
class Row(object):

    # rowTuple = (dumpFile, rowidx)

    @staticmethod 
    def printRow(rowTuple):
        from utils import inet_to_src
        from constants import L3SRC,L3DST,L4SRC,L4DST
        (dFile,idx) = rowTuple
        rowInfo = dFile.data[idx]
        # Check if it has DNS info?
        srcIP = inet_to_src(dFile.l3ids.reverse(rowInfo[L3SRC]))
        dstIP = inet_to_src(dFile.l3ids.reverse(rowInfo[L3DST]))
        srcPort = rowInfo[L4SRC]
        dstPort = rowInfo[L4DST]
        print 'srcIP: %s, dstIP: %s, srcPort: %d, dstPort: %d' %(srcIP,dstIP,srcPort,dstPort)

    @staticmethod
    def getTS(rowTuple):
        from constants import TS
        (dFile,idx) = rowTuple
        return dFile.data[idx][TS]

    @staticmethod
    def getDst(rowTuple):
        from constants import DIR,TX,RX,L3SRC,L3DST
        (dFile,idx) = rowTuple
        row = dFile.data[idx]
        if row[DIR] == TX:
            return row[L3DST]
        elif row[DIR] == RX:
            return row[L3SRC]
        else:
            return 'local'

    @staticmethod
    def getSrc(rowTuple):
        from constants import DIR,TX,RX,L3SRC,L3DST
        (dFile,idx) = rowTuple
        row = dFile.data[idx]
        if row[DIR] == TX:
            return row[L3SRC]
        elif row[DIR] == RX:
            return row[L3DST]
        else:
            return 'local'

    @staticmethod
    def extract(row):
        return True

    @staticmethod
    def setAdditionalData(rowTuple, rawPacket):
        (dFile,idx) = rowTuple
        rowAddData = dFile.addData.setdefault(rowTuple,dict())
        rowAddData['rawPacket'] = rawPacket

    @staticmethod
    def groupByFunc(rowTuple):
        from constants import DST
        (dFile,idx) = rowTuple
        return dFile.data[idx][DST]

    @staticmethod
    def loadRawPackets(master, groupByFunc = groupByFunc):
        groupedPackets = dict()
        #iter over sorted indexes
        for (fname,dFile) in master.indexes.items():
            # apply extract function
            exRowList = dFile.searchBy(extract)
            if len(exData) == 0:
                continue
            # Extract Indexes so we can pass into RawPacketLoader
            rowIndexes = [row[c.INDEX] for row in exRowList]
            rawPackets = dFile.loadPkts(rowIndexes)
            #iter over rawPackets
            for rowidx,rawPacket in rawPackets:
                rowTuple = (dFile, rowidx)
                #find the destination of Packet
                dst = groupByFunc(rowTuple)
                try:
                    # Place it into additional data about the row(Packet)
                    setAdditionalData(rowTuple, rawPacket)
                    # Group Action
                    groupedPackets.setdefault(dst,[]).append(rowTuple)
                except:
                    print 'Packet Issue with ', rowTuple
