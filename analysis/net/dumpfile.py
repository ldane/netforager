
""" 
DumpFile is a fast way to create header map for pcap files.
"""

#My Needs
from constants import *
from utils import *
from translator import Translator

#Generic Python libs
import dpkt
import numpy as np

class DumpFile(object):
    def __init__(self, fname, l2ids=Translator(), l3ids = Translator(), additionalData = None):
        self.fname = fname
        self.ignored = []
        self.l2ids = l2ids
        self.l3ids = l3ids
        self.addData = additionalData

    def __repr__(self):
        #TODO: fix
        return 'DumpFile(%s,duration=%s)' %(self.fname,self.duration())

    def __getitem__(self, index):
        return self.data[index]

    def parsePcap(self):

        from itertools import count
        from datetime import datetime as dt
        with open(self.fname, 'rb') as f:
            # go through pcap file to findout packet count
            numberOfPackets = self.lenPcap()
            # Allocate numpy array
            self.data = np.zeros((numberOfPackets,FIELD_LENGTH), dtype='i4')
            counter=count(start=0)
            pcapFile = dpkt.pcap.Reader(f)
            for (ts, pkt) in pcapFile:
                # Reset the locals
                l3pro = 0
                l4src = 0
                l4dst = 0

                # Increase the Index
                i=counter.next()
                curRow = self.data[i]
                curRow[TS] = int(ts)
                curRow[LEN] = len(pkt)
                curRow[INDEX]=i
                eth = dpkt.ethernet.Ethernet(pkt)
                
                # Fill L2
                # Mac address can not fit into integer!
                curRow[L2PRO] = eth.type
                curRow[L2SRC] = self.l2ids[eth.src]
                curRow[L2DST] = self.l2ids[eth.dst]
               
                # Fill L3
                # IPv4 can fit into 4byte integer
                # struct.unpack("i", ...)[0]
                # struct.pack("i", ...)
                if isinstance(eth.data, dpkt.ip.IP):
                    ip = eth.data
                    l3pro = ip.p
                    l3src = ip.src
                    l3dst = ip.dst
                elif isinstance(eth.data, dpkt.ip6.IP6):
                    ip6 = eth.data
                    l3pro = ip6.p
                    l3src = ip6.src
                    l3dst = ip6.dst
                elif isinstance(eth.data, dpkt.arp.ARP):
                    arp = eth.data
                    l3pro = arp.op
                    l3src = arp.spa
                    l3dst = arp.tpa
                    l4src = self.l3ids[arp.sha]
                    l4dst = self.l3ids[arp.tha]
                else:
                    self.ignored.append((ts,i,pkt))
                    continue
                curRow[L3PRO] = l3pro
                curRow[L3SRC] = self.l3ids[l3src]
                curRow[L3DST] = self.l3ids[l3dst]

                curRow[DIR] = detectDirection(l3src,l3dst)
                
                # Parse L4
                if isinstance(eth.data.data, dpkt.tcp.TCP):
                    tcp = eth.data.data
                    l4src = tcp.sport
                    l4dst = tcp.dport
                    curRow[TCPFLAGS] = tcp.flags
                    curRow[TCPTTL] = ip.ttl
                    # IF Needed
                    #option_list = dpkt.tcp.parse_opts ( tcp.opts )
                elif isinstance(eth.data.data, dpkt.udp.UDP):
                    udp = eth.data.data
                    l4src = udp.sport
                    l4dst = udp.dport
                    curRow[UDPCHKSUM] = udp.sum
                elif isinstance(eth.data.data, dpkt.icmp.ICMP):
                    icmp = eth.data.data
                    l4src = icmp.type
                    l4dst = icmp.code
                ## These are not right
                elif isinstance(eth.data.data, dpkt.igmp.IGMP):
                    igmp = eth.data.data
                    l4src = igmp.type
                    l4dst = igmp.sum
                elif isinstance(eth.data.data, dpkt.icmp6.ICMP6):
                    icmp6 = eth.data.data
                    l4src = icmp6.type
                    l4dst = icmp6.sum
                elif isinstance(eth.data.data, dpkt.gre.GRE):
                    gre = eth.data.data
                    l4src = eth.data.p
                    l4dst = gre.p
                # Fill L4 
                curRow[L4SRC] = l4src
                curRow[L4DST] = l4dst

                self.data[i]=curRow

    def save(self,fname):
        with open('./'+fname+'.np', 'wb') as f:
            np.save(f, self.data)

    @staticmethod
    def load(self):
        self.parsePcap()

    def groupBy(self, groupFunc):
        """Change grouping method and group again"""
        from collections import OrderedDict
        result=OrderedDict()
        for i in self.data:
            result.setdefault(groupFunc(i),[]).append(i)
        return result

    def searchBy(self, searchFunc):
        """Search and return the list"""
        return [i for i in self.data if searchFunc(i)]

    def formatBy(self, formatFunc):
        """Format Rows accordingly"""
        return [formatFunc(i) for i in self.data]

    def iterPkts(self, l):
        from itertools import count
        if not isinstance(l, list):
            raise Exception
        l.sort()
        with open(self.fname, 'rb') as f:
            counter = count(start=0)
            pcapFile = dpkt.pcap.Reader(f)
            lCursor = 0
            for (ts, pkt) in pcapFile:
                i = counter.next()
                if i == l[lCursor]:
                    eth = dpkt.ethernet.Ethernet(pkt)
                    yield (i,eth)
                    lCursor+=1
                    if lCursor >= len(l):
                        break

    def loadPkts(self, l):
        return [i for i in self.iterPkts(l)]

    def loadPkt(self, index):
        return self.loadPkts([index])
   
    def lenPcap(self):
        from itertools import count
        c = count(start=0)
        with open(self.fname, 'rb') as f:
            for i in dpkt.pcap.Reader(f):
                c.next()
        return c.next()

    def duration(self):
        from datetime import datetime
        end = datetime.fromtimestamp(self.data[-1][TS])
        start = datetime.fromtimestamp(self.data[0][TS])
        return end - start 
