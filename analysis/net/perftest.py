import timeit
import pcap
from guppy import hpy
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

from memory_profiler import profile

def testit(func):
    def func_wrapper(*args, **kwargs):
        from time import time
        hp = hpy()
        startMem = filterMem(hp.heap())
        startTime = time()
        (result,dns) = func(*args, **kwargs)
        finishTime = time()
        finishMem = filterMem(hp.heap())
        print "%s uses %d MB %lf seconds for %d/%d packets" % (func.__name__, finishMem - startMem, finishTime - startTime, result, dns)
    return func_wrapper

def filterMem(strRes):
    import re
    m = re.match(
        "Partition of a set of ([0-9]+) objects. Total size = ([0-9]+) bytes(.*)", str(strRes))
    objects = m.group(1)
    meminfo = int(m.group(2))/1024
    return meminfo

def randFile():
    import os, glob,random
    files = glob.glob(os.path.expanduser("~/data/openwrt/new-cont/out-00*.pcapng"))
    return random.choice(files)

