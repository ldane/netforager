#!/usr/bin/env python
# coding: utf-8
import dpkt, pcapy
from collections import OrderedDict

def pcapy_iter(fname, filterstr=None):
    pc = pcapy.open_offline(fname)
    if filterstr is not None:
        bpf = pcapy.compile(pcapy.DLT_EN10MB, 60000, filterstr, True, 0)
    h,b = pc.next()
    while h is not None:
        ts = h.getts()
        if filterstr is not None:
            if bpf.filter(b):
                yield (ts,b)
        else:
            yield (ts,b)
        h,b = pc.next()

def getHostname(buf):
    eth = dpkt.ethernet.Ethernet(buf)
    dhcp = dpkt.dhcp.DHCP(eth.data.data.data)
    return dict(dhcp.opts)[12]

def inet_to_str(inet):
    """Convert inet object to a string
        Args:
            inet (inet struct): inet network address
        Returns:
            str: Printable/readable IP address
    """
    # First try ipv4 and then ipv6
    import socket
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)

def main(fglob):
    # globals
    dhcpList = []
    dhcpv6List = []
    # build file list
    import glob
    flist = glob.glob(fglob)
    flist.sort()

    for fname in flist:
        for (ts,buf) in pcapy_iter(fname,'ip proto \udp and (port 67 or port 68)'):
            dhcpList.append((ts,buf))
    for fname in flist:
        for (ts,buf) in pcapy_iter(fname,'ip6 proto \udp and (port 546 or port 547)'):
            dhcpv6List.append((ts,buf))

    dhcpXid = OrderedDict()
    for (ts,dhcpPkt) in dhcpList:
        eth = dpkt.ethernet.Ethernet(dhcpPkt)
        dhcp = dpkt.dhcp.DHCP(eth.data.data.data)
        dhcpXid.setdefault(dhcp.xid,[]).append((ts,dhcpPkt))

    dhcpInfo = []
    for (xid,bufs) in dhcpXid.items():
        hostname = None
        dhcpip = None
        discover = False
        dhcpts = None
        for (ts,buf) in bufs:
            eth = dpkt.ethernet.Ethernet(buf)
            dhcp = dpkt.dhcp.DHCP(eth.data.data.data)
            dhcpOpts = dict(dhcp.opts)
            if 12 in dhcpOpts:
                hostname = dhcpOpts[12]
            if 53 in dhcpOpts:
                if dhcpOpts[53] == '\x01': #dhcpDiscover
                    discover = True
                if dhcpOpts[53] == '\x05': #dhcpack
                    dhcpip = eth.data.dst
                    dhcpts = ts
                if dhcpOpts[53] == '\x02': #dhcpoffer
                    pass
        if hostname is not None:
            if dhcpip:
                dhcpInfo.append([dhcpts,dhcpip,hostname,discover])

    discoverCount=0
    print 'File count:', len(flist)
    print 'DHCP transactions: ', len(dhcpXid)
    for dinfo in dhcpInfo:
        if dinfo[3] == True:
            discoverCount+=1
    print 'DHCP Discovers: ', discoverCount

#    print '\n'.join([str(info) for info in dhcpInfo])


    host2IP = {}
    for dinfo in dhcpInfo:
        target=host2IP.setdefault(dinfo[2],[])
        if dinfo[1] not in target:
            target.append([dinfo[0][0],dinfo[0][1],inet_to_str(dinfo[1]),dinfo[3]])

    import json
    with open('dhcplist.json','w') as f:
        json.dump(host2IP,f)

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        print 'Usage:\n dhcpinfo pcap'
        sys.exit(-1)
    main(sys.argv[1])
