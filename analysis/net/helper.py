
from dumpfile import DumpFile
from translator import Translator
from additionaldata import AdditionalData as AddData
from collections import OrderedDict


#import constants as const

class Helper(object):

    def __init__(self, foldername=None):
        import os
        import cPickle
        # Check if foldername is given?
        if foldername:
            self.rundir = foldername
            if os.path.isdir(foldername):
                self.newdir = False
            else:
                self.newdir = True            
        else:
            import constants as c
            self.newdir = True
            self.rundir = '%s/%s' %(c.DBFOLDER, os.getpid())

        if self.newdir:
            # Create the folder
            os.mkdir(self.rundir)
            # create global translators for Layer2, Layer3
            self.l2ids = Translator()
            self.l3ids = Translator()
            self.additionalData = AddData()
            self.metadata = {}
            self.metadata['l2'] = self.l2ids.uniqid
            self.metadata['l3'] = self.l3ids.uniqid
            #cPickle.save(metadata)
        else:
            self.metadata = cPickle.load()
            # Load translators from rundir
            self.l2ids = Translator.load(self.metadata['l2'])
            self.l3ids = Translator.load(self.metadata['l3'])

        # Change directory to rundir 
        os.chdir(self.rundir)


    def createFileList(self, regex=None):

        # If this is a previous run regex is not important, create filelist from dbfolder

        # Otherwise check regex and if it is None raise error
        # Create the FileList from Regex
        import glob

        # create file list with shell regex(glob)
        fileList = glob.glob(regex)
        self.metadata['regex'] = regex

        # This still might be relative
        # sort according to timestamp 
        #(Since I know they are ordered, I just order them according to their file name)
        self.fileList = sorted(fileList)


    def loadIndexes(self, verbose=False):
        try:
            self.indexes = OrderedDict()
            # Loop over fileList
            for i in self.fileList:
                # Create the object
                tempDF = DumpFile(i, self.l2ids, self.l3ids, self.additionalData)
                # Load the data into memory
                tempDF.parsePcap()
                #tempDF.load()
                basename = i.split('/')[-1]

                self.indexes[basename] = tempDF
                if verbose:
                    import sys
                    sys.stdout.write('.')

            # Else Load FileList
                # Loop over FileList
                # This is a new dbfolder so parse all the Pcap files
        except AttributeError:
            print 'fileList is empty, please Create File List'

    def save(self, verbose=False):
        import cPickle
        with open(self.rundir+'/metadata.json', 'w') as f:
            cPickle.dump(self.metadata,f)
            self.l2ids.save()
            self.l3ids.save()
            #TODO: sanitize the keys
            #os.path.basename
            for key,index in self.indexes.iteritems():
                index.save(key)

    def filelen(self):
        return len(self.fileList)
    
    def __len__(self):
        # How to determine which one to use?
        return len(self.indexes)
