#!/usr/bin/env python
# coding: utf-8
import dpkt
from collections import OrderedDict

def buildDNSTransactions(filename):
    stack = OrderedDict()
    with open(filename, 'rb') as f:
        for (ts,pkt) in dpkt.pcap.Reader(f):
            eth = dpkt.ethernet.Ethernet(pkt)
            if isinstance(eth.data, dpkt.ip.IP):
                ip = eth.data
                if isinstance(ip.data, dpkt.udp.UDP):
                    udp = ip.data
                    if 53 in (udp.sport, udp.dport):
                        dns = dpkt.dns.DNS(udp.data)
                        transaction_id = dns.id
                        reqres = [udp.dport,udp.sport].index(53)
                        stack.setdefault(transaction_id,[None,None])[reqres] = dns
            continue
    return stack

def dns2str(dns):
    if dns is None:
        return None
    transaction_id = dns[0]
    dnsreq = dns[1][0] #dns request
    dnsres = dns[1][1] #dns response
    v=4
    if dnsreq.qd[0].type == 28:
        v=6
    dnsname = dnsreq.qd[0].name
    return '%d %s (v%d)' %(transaction_id, dnsname, v)

def buildTCPConversations(filename):
    stack = OrderedDict()
    with open(filename, 'rb') as f:
        for (ts,pkt) in dpkt.pcap.Reader(f):
            eth = dpkt.ethernet.Ethernet(pkt)
            if isinstance(eth.data, dpkt.ip.IP):
                ip = eth.data
                if isinstance(ip.data, dpkt.tcp.TCP):
                    tcp = ip.data
                    src=ip.src
                    dst=ip.dst                
                    sport = tcp.sport
                    dport = tcp.dport
                    #check if it's already in
                    TX=((src,sport),(dst,dport))
                    RX=((dst,dport),(src,sport))
                    if TX in stack:
                        stack[TX].append((ts,pkt))
                    elif RX in stack:
                        stack[RX].append((ts,pkt))
                    else:
                        stack.setdefault(TX,[]).append((ts,pkt))
    return stack

def inet_to_str(inet):
    """Convert inet object to a string
        Args:
            inet (inet struct): inet network address
        Returns:
            str: Printable/readable IP address
    """
    # First try ipv4 and then ipv6
    import socket
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)

def conv2SNI(conversation):
    for (ts,packet) in conversation[1]:
        eth = dpkt.ethernet.Ethernet(packet)
        ip = eth.data
        tcp = ip.data
        if len(tcp.data) <= 0:
            continue
        # we only care about handshakes for now...
        if ord(tcp.data[0]) != 22:
            continue
        try:
            tls = dpkt.ssl.TLS(tcp.data)
        except dpkt.dpkt.NeedData, e:
            # TODO: meeeeh
            continue

        for record in tls.records:
            # TLS handshake only
            if record.type != 22:
                continue
            if len(record.data) == 0:
                continue
            # Client Hello only
            if ord(record.data[0]) != 1:
                continue

            handshake = dpkt.ssl.TLSHandshake(record.data)
            if not isinstance(handshake.data, dpkt.ssl.TLSClientHello):
                CONtinue
            ch = handshake.data
            for extension in ch.extensions:
                if extension[0]==0:
                    return extension[1][5:]
                    break
            break

def conv2str(conversation):
    if conversation is None:
        return None
    ((src,sport),(dst,dport)) = conversation[0]
    dst = inet_to_str(dst)
    name = conv2SNI(conversation)
    return '(%30s:%-5d), Packets %3d, Bytes %6d' %(name,dport, len(conversation[1]),conv2size(conversation))    


def conv2ipstr(conversation):
    if conversation is None:
        return None
    ((src,sport),(dst,dport)) = conversation[0]
    src = inet_to_str(src)
    dst = inet_to_str(dst)
    return '(%15s,%-5d) (%15s,%5d), Packets %d, Bytes %d' %(src,sport,dst,dport, len(conversation[1]),conv2size(conversation))    

def conv2rawsize(conversation):
    total=0
    for ts,packet in conversation[1]:
        total+=len(packet)
    return total

def conv2size(conversation):
    return conv2rxsize(conversation, False)


def conv2rxsize(conversation, rx=True):
    if conversation is None:
        return 0
    total=0
    for ts,packet in conversation[1]:
        eth=dpkt.ethernet.Ethernet(packet)
        ip = eth.data
        tcp = ip.data
        if rx==True and tcp.sport == 443:
            total+=ip.len-tcp.off
        else:
            total+=ip.len-tcp.off
    return total

def conv2TS(convs):
    if convs is None:
        return None
    ((src,sport),(dst,dport)) = convs[0]
    (firstTS,firstpacket) = convs[1][0]
    (lastTS,lastpacket) = convs[1][-1]
    name = conv2SNI(convs)
    return "%.6f, %30s, %.6f" %(firstTS,name,lastTS)

def conv2len(convs):
    if convs is None:
        return None
    return len(convs[1])

def conv2rxlen(convs):
    if convs is None:
        return 0

    total=0
    for ts,packet in convs[1]:
        eth=dpkt.ethernet.Ethernet(packet)
        ip = eth.data
        tcp = ip.data
        if tcp.sport == 443:
            total+=1
    return total

def domainBucket(arg):
    nameBucket={}
    for item in arg.items():
        name = conv2SNI(item)
        nameBucket.setdefault(name,{"byte":0,"packet":0})
        nameBucket[name]['byte']+=conv2rxsize(item)
        nameBucket[name]['packet']+=conv2rxlen(item)
    return nameBucket


def main(pcap1,pcap2):

### DNS info
    dnsStack1 = buildDNSTransactions(pcap1)
    dnsStack2 = buildDNSTransactions(pcap2)

    print "DNS count\n", len(dnsStack1), len(dnsStack2)

    print 'DNS requests'
    for (item1,item2) in map(None,dnsStack1.items(),dnsStack2.items()):
        print "%50s | %s" %(dns2str(item1), dns2str(item2))


### follow cnames to ip


### group conversations
    convs1 = buildTCPConversations(pcap1)
    convs2 = buildTCPConversations(pcap2)

    size1=[conv2size(i) for i in convs1.items()]
    size2=[conv2size(i) for i in convs2.items()]

    print 'TCP Conversations count\n', len(convs1.items()), len(convs1.items())
    for (item1, item2) in map(None,convs1.items(),convs2.items()):
        print "%70s | %s" %(conv2ipstr(item1), conv2ipstr(item2))
    
    print 'TCP Conversation sizes\n', sum(size1), sum(size2)
    for (item1, item2) in map(None,size1,size2):
        print "%10s | %s" %(item1, item2)

    print 'TCP Conversations with SNI\n', len(convs1.items()), len(convs1.items())
    for (item1, item2) in map(None,convs1.items(),convs2.items()):
        print "%70s | %s" %(conv2str(item1), conv2str(item2))

    print 'Conversation TS\n'
    for (item1, item2) in map(None,convs1.items(),convs2.items()):
        print "%70s | %s" %(conv2TS(item1), conv2TS(item2))

    print '\n\nTotal Packet size'
    print '===== Filename: ', pcap1
    print '\n'.join(['%s, %d' %(domain,buc['byte']) for domain,buc in domainBucket(convs1).items()])
    print '===== Filename: ', pcap2
    print '\n'.join(['%s, %d' %(domain,buc['byte']) for domain,buc in domainBucket(convs2).items()])


    print '\n\nAverage Packet size'
    print '===== Filename: ', pcap1
    print '\n'.join(['%s, %d' %(domain,buc['byte']/buc['packet']) for domain,buc in domainBucket(convs1).items()])
    print '===== Filename: ', pcap2
    print '\n'.join(['%s, %d' %(domain,buc['byte']/buc['packet']) for domain,buc in domainBucket(convs2).items()])


    print '\n\nPacket Count vs URL'
    print '===== Filename: ', pcap1
    print '\n'.join(['%s, %d' %(domain,buc['packet']) for domain,buc in domainBucket(convs1).items()])
    print '===== Filename: ', pcap2
    print '\n'.join(['%s, %d' %(domain,buc['packet']) for domain,buc in domainBucket(convs2).items()])


if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        print 'Usage:\n compare pcap1 pcap2'
        sys.exit(-1)
    main(sys.argv[1],sys.argv[2])
