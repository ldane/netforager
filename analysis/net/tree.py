
class Node(object):

    def __init__(self, name, value, dumpfile, dumpfile_idx):
        self._name = name
        self.value = value
        self.parent = dumpfile
        self.idx = dumpfile_idx
        self.score = 0

    def __hash__(self):
        return hash(self.name)

    def inc(self):
        self.score+=1

    @property
    def name(self):
        return self._name

class Tree(object):

    def __init__(self):
        self.childs =  defaultdict(Tree)
        self.node = None

    def assignNode(self, node):
        if not isinstance(node,Node):
            raise Exception
        self.node = node

    def __getattr__(self, attr):
        return self.childs[attr]

    def __setattr(self, attr, val):
        self.childs[attr] = val

    def inc(self):
        self.node.inc()

    def ptr(self, depth = 0):
        """ print a tree """
        for k in self.childs.keys():
            #print("%s %2d %s %d" % ("".join(depth * ["    "]), depth, k, self.childs[k].score))
            print("%s %s %d" % ("".join(depth * ["    "]), k, self.childs[k].score))
            self.childs[k].ptr(depth+1)

