
import constants as c

class Translator(object):
    def __init__(self):
        from itertools import count
        import uuid
        self.lastidx = count(1)
        self._data =  dict()
        self._inv = dict()
        self.uniqid = str(uuid.uuid4())

    def __getitem__(self, item):
        value = self._data.get(item, None)
        if value == None:
            value = self.lastidx.next()
        self._data[item] = value
        self._inv[value] = item
        return value

    def __setitem__(self, item, value):
        print 'Can\'t set an item on Translator'
        raise Exception

    def __contains__(self, item):
        return (item in self._data) or (item in self._inv)

    def reverse(self, value):
        return self._inv[value]

    def save(self):
        import cPickle
        with open(self.uniqid, 'w') as f:
            cPickle.dump(self._data,f)

    @staticmethod
    def load(uniqid):
        import cPickle
        with open(uniqid, 'r') as f:
            newobj = Translator()
            newobj._data = cPickle.load(f)
            newobj._inv = {v:k for k,v in newobj._data.iteritems()}

            return newobj

    def checkConsistency(self):
        return len(self._data) == len(self._inv)

    def __len__(self):
        return len(self._data)

