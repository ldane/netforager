"""All of the constant values"""

FIELD_LENGTH   = 15
PCAP_LENGTH    = 100000

#FIELD Indexes
#{
# MetaData
TS             = 0
LEN            = 1
DIR            = 2
INDEX          = 3
GID            = 4

# Actual Data
L2PRO          = 5
L2SRC          = 6
L2DST          = 7
L3PRO          = 8
L3SRC          = 9
L3DST          = 10
L4SRC          = 11
L4DST          = 12

TCPFLAGS       = 13
UDPCHKSUM      = 13
TCPTTL         = 14

#}

#ROW Definition

ROW_DEF = 'i4, i4, i4, i4, i4, i4, i4, i4, i4, i4, i4, i4, i4, i4'


# DBFOLDER
DBFOLDER = '/home/ldane/data/run'


#FIELD VALUES

#DIRECTION
TX             = 1
RX             = -1
# Don't use 0, 0 is default element
LOCAL          = 2

#ETHPROTO
IP             = 0x0800
IPv6           = 0x86dd

#IPPROTO
ICMP           = 1
TCP            = 6
UDP            = 17
IP6IN4         = 41
GRE            = 47 

