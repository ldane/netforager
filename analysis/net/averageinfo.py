#!/usr/bin/env python
# coding: utf-8

import dpkt, pcapy
from collections import OrderedDict
import datetime as dt


def pcapy_iter(fname, filterstr=None):
    pc = pcapy.open_offline(fname)
    if filterstr is not None:
        bpf = pcapy.compile(pcapy.DLT_EN10MB, 60000, filterstr, True, 0)
    h,b = pc.next()
    while h is not None:
        ts = h.getts()
        if filterstr is not None:
            if bpf.filter(b):
                yield (ts,b)
        else:
            yield (ts,b)
        h,b = pc.next()

def getHostname(buf):
    eth = dpkt.ethernet.Ethernet(buf)
    dhcp = dpkt.dhcp.DHCP(eth.data.data.data)
    return dict(dhcp.opts)[12]

def inet_to_str(inet):
    """Convert inet object to a string
        Args:
            inet (inet struct): inet network address
        Returns:
            str: Printable/readable IP address
    """
    # First try ipv4 and then ipv6
    import socket
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)


fglob='/home/ldane/data/openwrt/new-cont/out-*.pcapng'





#load dhcpinfo
import json
with open('/home/ldane/data/openwrt/new-cont/dhcplist.json', 'r') as f:
    dhcpList = json.load(f)

ip2host={}
for host, dhcpInfo in dhcpList.items():
    for dinfo in dhcpInfo:
        target=ip2host.setdefault(dinfo[2],[])
        target.append([dinfo[0],dinfo[1],host,dinfo[3]])


def dhcpQuery(ts,ip):
    if ip not in ip2host:
        return None
    target=ip2host[ip]
    ts = float('%d.%d' %(ts[0],ts[1]))
    maxCount=len(target)
    i=0
    if float('%d.%d' %(target[i][0], target[i][1])) > ts:
        return target[i][2]
    while i < maxCount-1:
        currts= float('%d.%d' %(target[i][0], target[i][1]))
        nextts= float('%d.%d' %(target[i+1][0], target[i+1][1]))
        if currts < ts and nextts > ts:
            return target[i][2]
        i+=1
    return None



# build file list
import glob
flist = glob.glob(fglob)
flist.sort()


#Calculate the total days and total hours
for (ts,buf) in pcapy_iter(flist[0]):
    ts=dt.datetime.fromtimestamp(ts[0])
    print ts
    break
ts1=ts
for (ts,buf) in pcapy_iter(flist[-1]):
    continue
    break
ts=dt.datetime.fromtimestamp(ts[0])
print ts
ts2=ts
t=ts2-ts1
print t
totalD = t.days
totalH = t.total_seconds()/(60*60)

#Globals

# total Average
bRX = {}
bTX = {}
bRXCount = {}
bTXCount = {}

#Initialize Globals
for host in dhcpList:
    bRX[host]={}
    bTX[host]={}
    bRXCount[host]={}
    bTXCount[host]={}

for fname in flist:
    for (ts,buf) in pcapy_iter(fname):
        # Packet Length
        pktlen = len(buf)
        
        # Find the target
        pkt = dpkt.ethernet.Ethernet(buf)
        if not isinstance(pkt.data, dpkt.ip.IP):
            continue
        ip=pkt.data
        
        rx=tx=False
        if '192.168.1' not in inet_to_str(ip.src):
            target = inet_to_str(ip.dst)
            rx=True
        elif '192.168.1' not in inet_to_str(ip.dst):
            target = inet_to_str(ip.src)
            tx=True
        else:
            continue
        
        host = dhcpQuery(ts, target)
        if host == None:
            #print 'Ups', ts, target
            continue

        ts=dt.datetime.fromtimestamp(ts[0])
        tday = str(ts.date())
        thour = ts.hour

        if rx:
            t=bRX[host]
            tC=bRXCount[host]
        if tx:
            t=bTX[host]
            tC=bTXCount[host]

        t.setdefault((tday,thour),0)
        t[(tday,thour)]+=pktlen
        tC.setdefault((tday,thour),0)
        tC[(tday,thour)]+=1

