import sys, os
sys.path.append(os.path.expanduser('~/netuser/'))
from helper import Helper
import constants as c
from utils import isSorted

# Create helper
master = Helper()

# fill with filenames
master.createFileList('/home/ldane/data/openwrt/new-cont/out-*.pcapng')
print '%d files.' %(master.filelen())

# create numpy indexes
master.loadIndexes(verbose=True)

print '\nIndexes Loaded\n'

# save into rundir
master.save()
print 'Rundir: %s,\nMeta:\n%s' %( master.rundir, master.metadata)

# check if it is sorted
assert isSorted(master.indexes.iteritems())
# if not sorted sort
# Sort according to filename
#res=[value for (key, value) in sorted(master.indexes.items())]

#Helper functions

def cacheIP2DNS(src,l3id,ts):
    result=[]
    for i in cacheIndex.l3id[src][l3id]:
        #ignore cnames
        if cacheInfo[i][3] == 1:
            #check if it is valid (not expired)
            if cacheInfo[i][0]+cacheInfo[i][2]>= ts and cacheInfo[i][0] <= ts:
                # no need to duplicate
                if cacheInfo[i][1] not in result:
                    result.append(cacheInfo[i][1])
    return result

def createIP2ASN(master):
    import pyasn
    asndb=pyasn.pyasn('/home/ldane/ipasn.20170125.dat','/home/ldane/asnames.json')
    data = {}
    for (ip,l3id) in master.l3ids._data.iteritems():
        try:
            l3ip = utils.inet_to_str(ip)
        except:
            continue
        try:
            data[l3id] = asndb.lookup(l3ip)[0]
        except:
            data[l3id] = None
    return data

# Start DNS Processing

print 'Load Raw DNS Packets'

#from collections import OrderedDict
from dpkt.dns import DNS
from dnsRow import DNSRow


DNSPacketsPerSRC = dict()
#iter over sorted file list
for (fname,dFile) in master.indexes.items():
    # Extract Only DNS Rows
    dnsData = dFile.searchBy(DNSRow.extract)
    if len(dnsData) == 0:
        continue
    # Extract Indexes so we can pass into RawPacketLoader
    rowIndexes = [row[c.INDEX] for row in dnsData]
    rawPackets = dFile.loadPkts(rowIndexes)
    #iter over rawPackets
    for rowidx,rawDPacket in rawPackets:
        rowTuple = (dFile, rowidx)
        #find the destination of DNS Packet
        dst = DNSRow.getTarget(rowTuple)
        try:
            # try to parse DNS payload
            dnsPacket = DNS(rawDPacket.data.data.data)
            # Place it into additional data about the row(Packet)
            DNSRow.setDNSData(rowTuple, dnsPacket)
            # Place it into 
            DNSPacketsPerSRC.setdefault(dst,[]).append(rowTuple)
        except:
            print 'MalFormed Packet', rowTuple


print 'Group DNS packets'

# Group DNS Packets (in reverse order)
DNSPacketsPerSRCGrouped=dict()
for src, queryList in DNSPacketsPerSRC.items():
    for queryT in queryList:
        (dFile, idx) = queryT
        prevQueryList = DNSPacketsPerSRCGrouped.setdefault(src,[])
        placed = False
        for (pdFile,pidx,groupedList) in prevQueryList:
            pqueryT = (pdFile,pidx)
            if DNSRow.getDomainfromRow(queryT) == DNSRow.getDomainfromRow(pqueryT):
                # If they are closer than 2 second
                if DNSRow.getTSfromRow(queryT) - DNSRow.getTSfromRow(pqueryT) <= 2:
                    placed = True
                    groupedList.append(queryT)
                    break
                #Failsafe
                if dFile[idx][c.TS] - pdFile[pidx][c.TS] > 10:
                    break
        if not placed:
            prevQueryList.insert(0,(dFile,idx,[]))


# Reverse them back
for src,queryList in DNSPacketsPerSRCGrouped.items():
    DNSPacketsPerSRCGrouped[src]=queryList[::-1]



# Print the IPs with DNS queries
import utils
print 'DNS queries'
for src,queryList in DNSPacketsPerSRCGrouped.items():
    print 'l3id: %d, IP: %s, DNSReq: %d' %( src, utils.inet_to_str(master.l3ids.reverse(src)), len(queryList))

print 'Build Cache Info'

cacheInfo = []
for src,queryList in DNSPacketsPerSRC.items():
    for queryT in queryList:
        dFile, idx = queryT
        if not DNSRow.isQuery(queryT):
            query = DNSRow.getDNSData(queryT)
            rowTS = DNSRow.getTSfromRow(queryT)
            for dnsAnswer in query.an:
                if dnsAnswer.type ==5:
                    ret = dnsAnswer.cname
                if dnsAnswer.type ==1:
                    ret = dnsAnswer.ip
                res = (rowTS, query.qd[0].name, dnsAnswer.ttl, dnsAnswer.type, ret, queryT)
                cacheInfo.append(res)


print 'Build Cache Index'
cacheIndex = type('cacheIndex',(object,), {'name': dict(), 'ts': dict(), 'l3id': dict()})

for idx,cache in enumerate(iter(cacheInfo)):
    (rowTS, qName, dTTL, dType, ansRes, queryT) = cache
    # find src
    src = DNSRow.getTarget(queryT)
    
    cacheIndex.name.setdefault(src, dict())
    cacheIndex.ts.setdefault(src, [])
    cacheIndex.l3id.setdefault(src, dict())
    
    query = DNSRow.getDNSData(queryT)
    cacheIndex.name[src].setdefault(qName,[]).append(idx)
    cacheIndex.ts[src].append(idx)
    if dType == 1:
        if ansRes in master.l3ids:
            cacheIndex.l3id[src].setdefault(master.l3ids._data[ansRes],[]).append(idx)

# Find Sources that requested youtube
srcWithYoutube=[]
for src,queryList in sorted(DNSPacketsPerSRCGrouped.items()):
    for queryT in queryList:
        rowTuple = queryT[0:2]
        if 'www.youtube.com' in DNSRow.getDomainfromRow(rowTuple):
            srcWithYoutube.append(src)
            break


# Find the Starting Points in the DNS queries
startPositions = dict()
for src in srcWithYoutube:
    queryList = DNSPacketsPerSRCGrouped[src]
    #Find staring points for youtube
    for queryT in queryList:
        rowTuple = queryT[0:2]
        if 'www.youtube.com' in DNSRow.getDomainfromRow(rowTuple):
            if DNSRow.isQuery(rowTuple):
                idxOfQuery = queryList.index(queryT)
                startPositions.setdefault(src,[]).append(idxOfQuery)


# Count every query from starting point until 10 seconds
deltaTS = 10
counts = dict()
test = 0
for src,startList in startPositions.items():
    queryList = DNSPacketsPerSRCGrouped[src]
    counts[src] = dict()
    for startPosition in startList:
        i=1
        startTS = DNSRow.getTSfromRow(queryList[startPosition][0:2])
        curQuery = queryList[startPosition+i][0:2]
        while DNSRow.getTSfromRow(curQuery) - startTS < deltaTS:
            # Get the dns name from Query
            dnsname = DNSRow.getDomainfromRow(curQuery)
            # get the current value or if not 0
            value = counts[src].setdefault(dnsname,0)
            # increase the value and assign it back
            counts[src][dnsname] = value+1
            # Move to the next query
            i+=1
            if len(queryList) <= startPosition+i:
                break            
            curQuery = queryList[startPosition+i][0:2]



from collections import Counter
dnsCounters = dict()
for src,countList in counts.items():
    dnsCounters[src] = Counter(counts[src])
    print "%s requested youtube %d times" %(utils.inet_to_str(master.l3ids.reverse(src)),len(startPositions[src]))
    print "\n".join(["\t%s %d" %(i,j) for i,j in dnsCounters[src].most_common(15)])


commonSubset = dict()
for src,countList in counts.items():
    dnsCounters[src] = Counter(countList)
    for dnsName,count in dnsCounters[src].items():
        value = commonSubset.setdefault(dnsName,0)
        commonSubset[dnsName]=value+count

target = sorted(commonSubset, key = commonSubset.get, reverse=True)[0:15]

for i,name in enumerate(target):
    print i, name

deltaTS = 10
targetOrder = dict()
targetSeq = dict()
for src,startList in startPositions.items():
    queryList = DNSPacketsPerSRCGrouped[src]
    targetOrder[src] = dict()
    targetSeq[src] = dict()
    for startPosition in startList:
        i=1
        startTS = DNSRow.getTSfromRow(queryList[startPosition][0:2])
        curQuery = queryList[startPosition+i][0:2]
        while DNSRow.getTSfromRow(curQuery) - startTS < deltaTS:
            # Get the dns name from Query
            dnsname = DNSRow.getDomainfromRow(curQuery)
            # if dnsname in Target
            if dnsname in target:
                targetOrder[src].setdefault(dnsname,[]).append(i)
                targetSeq[src].setdefault(startPosition,[]).append(target.index(dnsname))
            i+=1
            if len(queryList) <= startPosition+i:
                break
            curQuery = queryList[startPosition+i][0:2]


relativePosInfo = [[(0,0) for x in target] for y in target] 
for src,seqs in targetSeq.items():
    for startPosition,seq in seqs.items():
        # Relative position determination
        for currentPos,currentID in enumerate(seq):
            for targetPos,targetID in enumerate(seq):
                (L,R) = relativePosInfo[currentID][targetID]
                if targetPos > currentPos:
                    L = L+1
                elif targetPos < currentPos:
                    R = R+1
                relativePosInfo[currentID][targetID] = (L,R)


for x in range(len(target)):
    print ' '.join('%02d-%02d' %(y[0],y[1]) for y in relativePosInfo[x])


### Create summary info
"""
from collections import OrderedDict
import codecs
src2ASN={}
for (fname,dFile) in master.indexes.iteritems():
    res = dFile.groupBy(convGroup)
    sortedGroups = OrderedDict()
    for convID, pktArray in res.iteritems():
        t = pktArray[0]
        if t[c.DIR] == c.TX:
            l3src=t[c.L3SRC]
            l3dst=t[c.L3DST]
        elif t[c.DIR] == c.RX:
            l3src=t[c.L3DST]
            l3dst=t[c.L3SRC]
        else:
            "It is local packet"
            continue
        target = sortedGroups.setdefault(l3src,OrderedDict())[convID]=pktArray
        

    with codecs.open(fname+".sum","w","utf-8") as f:
        for src,groupDict in sortedGroups.iteritems():
            f.write("============== SRC: %s ==============\n" %(utils.inet_to_str(master.l3ids.reverse(src))))
            for convID,pktArray in groupDict.iteritems():
                t = pktArray[0]
                if t[c.DIR] == c.TX:
                    l3dst=t[c.L3DST]
                    l4dst=t[c.L4DST]
                elif t[c.DIR] == c.RX:
                    l3dst=t[c.L3SRC]
                    l4dst=t[c.L4SRC]
                else:
                    "It is local packet"
                    continue
                asndst = asnIP[l3dst]
                try:
                    l3dns = cacheIP2DNS(src,l3dst,t[c.TS])
                except:
                    l3dns = []
                l3dst = utils.inet_to_str(master.l3ids.reverse(l3dst))                

                src2ASN.setdefault(src,[])
                if asndst not in src2ASN[src]:
                    src2ASN[src].append(asndst)
                    
                f.write("TS:%d, pkt:%5d, L3: %40s : %5d, DNS:%s ASN: %s (%s)\n" %(t[c.TS],len(pktArray), l3dst, l4dst,l3dns, asndb.get_as_name(asndst), asndst))
"""
