#!/usr/bin/env python
# coding: utf-8
import dpkt
import pcapy
import datetime as dt
from collections import OrderedDict

def pcapy_iter(fname, filterstr=None):
    pc = pcapy.open_offline(fname)
    if filterstr is not None:
        bpf = pcapy.compile(pcapy.DLT_EN10MB, 60000, filterstr, True, 0)
    h,b = pc.next()
    while h is not None:
        ts = h.getts()
        if filterstr is not None:
            if bpf.filter(b):
                yield (ts,b)
        else:
            yield (ts,b)
        h,b = pc.next()

def checkSSL(buf):
    eth=dpkt.ethernet.Ethernet(buf)
    if isinstance(eth.data,dpkt.ip.IP) or isinstance(eth.data, dpkt.ip6.IP6):
        ip = eth.data
        if isinstance(ip.data, dpkt.tcp.TCP): #or isinstance(ip.data, dpkt.udp.UDP):
            payload = ip.data
            try:
                dpkt.ssl.TLS(payload.data)
                return True
            except:
                pass
    return False

def checkPort(convID, portList):
    for port in portList:
        if port in convID[1]:
            return True

def checkHTTP(convID):
    portList=[80, 443, 8080]
    return checkPort(convID, portList)

def checkDNS(convID):
    portList=[53]
    return checkPort(convID, portList)


def checkMail(convID):
    portList=[25, 110, 143, 465, 587, 993, 995]
    return checkPort(convID, portList)
    
def reverseConvID(convID):
    return (convID[0][::-1], convID[1][::-1])


def getconvID(buf):
    eth=dpkt.ethernet.Ethernet(buf)
    if isinstance(eth.data,dpkt.ip.IP) or isinstance(eth.data, dpkt.ip6.IP6):
        ip = eth.data
        if isinstance(ip.data, dpkt.tcp.TCP): # or isinstance(ip.data, dpkt.udp.UDP):
            payload = ip.data
            convID = ((ip.src, ip.dst), (payload.sport, payload.dport))
            return convID
        elif isinstance(ip.data, dpkt.udp.UDP):
            payload = ip.data
            if 53 in (payload.sport, payload.dport):
                return 'DNS'
            else:
                return 'UDP'
    return None
 

def main(fglob):
    # globals
    conversations = OrderedDict()
    ignored=0
    pktcount=0
    dnscount=0
    udpcount=0
    tcpcount=0
    # build file list 
    import glob
    flist = glob.glob(fglob)
    flist.sort()


    for (ts,buf) in pcapy_iter(flist[0]):
        ts=dt.datetime.fromtimestamp(ts[0])
        print 'Start-Time:', ts
        break

    for (ts,buf) in pcapy_iter(flist[-1]):
        continue
        break
    ts=dt.datetime.fromtimestamp(ts[0])
    print 'End-Time:', ts

    for fname in flist:
        for (ts,buf) in pcapy_iter(fname):
            pktcount+=1
            convID = getconvID(buf)
            if convID == None:
                ignored+=1
                continue
            elif convID == 'DNS': 
                dnscount+=1
                continue
            elif convID == 'UDP':
                udpcount+=1
                continue
            elif reverseConvID(convID) in conversations:
                convID = reverseConvID(convID)
            tcpcount+=1
            conversations.setdefault(convID,False)
            if conversations[convID] == False:
                if checkSSL(buf):
                    conversations[convID] = True

    tlscount=0
    httpcount=0
    mailcount=0
    for convsID, tlscheck in conversations.iteritems():
        if convsID in ['UDP', 'DNS']:
            continue
        if tlscheck == True:
            tlscount+=1
        if checkHTTP(convsID):
            httpcount+=1
        if checkMail(convsID):
            mailcount+=1

    print 'Total files        : ', len(flist)
    print 'Total packets      : ', pktcount 
    print 'TCP   packets      : ', tcpcount
    print 'DNS   packets      : ', dnscount
    print 'Other UDP packets  : ', udpcount
    print 'Other packets      : ', ignored
    print 'Total conversations: ', len(conversations)
    print 'TLS   conversations: ', tlscount
    print 'HTTP  conversations: ', httpcount
    print 'Mail  conversations: ', mailcount


if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        print 'Usage:\n genericinfo pcap'
        sys.exit(-1)
    main(sys.argv[1])
