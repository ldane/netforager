#!/usr/bin/env python
# coding: utf-8
import dpkt, pcapy
from collections import OrderedDict

def pcapy_iter(fname, filterstr=None):
    pc = pcapy.open_offline(fname)
    if filterstr is not None:
        bpf = pcapy.compile(pcapy.DLT_EN10MB, 60000, filterstr, True, 0)
    h,b = pc.next()
    while h is not None:
        ts = h.getts()
        if filterstr is not None:
            if bpf.filter(b):
                yield (ts,b)
        else:
            yield (ts,b)
        h,b = pc.next()

def getHostname(buf):
    eth = dpkt.ethernet.Ethernet(buf)
    dhcp = dpkt.dhcp.DHCP(eth.data.data.data)
    return dict(dhcp.opts)[12]

def inet_to_str(inet):
    """Convert inet object to a string
        Args:
            inet (inet struct): inet network address
        Returns:
            str: Printable/readable IP address
    """
    # First try ipv4 and then ipv6
    import socket
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)

def main(fglob):
    # globals
    multicast = []
    # build file list
    import glob
    flist = glob.glob(fglob)
    flist.sort()

    for fname in flist:
        for (ts,buf) in pcapy_iter(fname,'ether host not 14:91:82:b9:68:56 and ether host not b8:27:eb:12:6a:f7'):
            multicast.append([ts[0],ts[1],buf])

    print len(multicast)

    ip2mac={}
    mac2host={}
    for x,y,buf in multicast:
        pkt=dpkt.ethernet.Ethernet(buf)
        if not isinstance(pkt.data, dpkt.ip.IP):
            continue
        ip=pkt.data
        target=ip2mac.setdefault(inet_to_str(ip.src),[])
        if pkt.src not in target:
            target.append(pkt.src)
        target=ip2mac.setdefault(inet_to_str(ip.dst),[])
        if pkt.dst not in target:
            target.append(pkt.dst)
            
        if not isinstance(ip.data, dpkt.udp.UDP):
            continue
        udp=ip.data
        if 67 not in [udp.sport,udp.dport]:
            continue
        dhcp = dpkt.dhcp.DHCP(udp.data)
        if 12 in dict(dhcp.opts):
            host=dict(dhcp.opts)[12]
            if udp.sport == 68:
                mac2host[pkt.src]=host


    print '======'
    print ip2mac
    print '======'
    print mac2host

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        print 'Usage:\n multicast pcap'
        sys.exit(-1)
    main(sys.argv[1])
