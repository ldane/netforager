
class Extractor(object):

    def __init__(self):
        pass
    
    def extract(self):
        pass

class PlainTXTExtractor(Extractor):
    pass

class POFExtractor(Extractor):
    pass

class DHCPMovement(Extractor):
    pass


class DNSRaw(Extractor):

    @staticmethod
    def extract(dumpfile):
        #DNSPacketsPerSRC = dict()
        # Extract Only DNS Rows
        from dnsrow import DNSRow
        dnsData = dumpfile.searchBy(DNSRow.extract)
        if len(dnsData) == 0:
            return
        # Extract Indexes so we can pass into RawPacketLoader
        rowIndexes = [row[c.INDEX] for row in dnsData]
        rawPackets = dFile.loadPkts(rowIndexes)
        #iter over rawPackets
        for rowidx,rawDPacket in rawPackets:
            rowTuple = (dFile, rowidx)
            #find the destination of DNS Packet
            dst = DNSRow.getTarget(rowTuple)
            try:
                # try to parse DNS payload
                dnsPacket = DNS(rawDPacket.data.data.data)
                # Place it into additional data about the row(Packet)
                DNSRow.setDNSData(rowTuple, dnsPacket)
                # Place it into
                DNSPacketsPerSRC.setdefault(dst,[]).append(rowTuple)
            except:
                print 'MalFormed Packet', rowTuple
