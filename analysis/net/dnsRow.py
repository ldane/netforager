from row import Row

class DNSRow(Row):    
    @staticmethod
    def getDNSData(rowIns):
        (dFile,idx) = rowIns
        return dFile.addData[rowIns]['dnsPacket']

    @staticmethod
    def setDNSData(rowIns,dnsData):
        (dFile,idx) = rowIns
        rowAddData = dFile.addData.setdefault(rowIns,dict())
        rowAddData['dnsPacket'] = dnsData

    @staticmethod 
    def printRow(rowIns):
        from utils import inet_to_str
        from constants import TS
        (dFile,idx) = rowIns
        # Check if it has DNS info?
        query = DNSRow.getDNSData(rowIns) 
        src = DNSRow.getTarget(rowIns)
        inetsrc = dFile.l3ids.reverse(src)
        dnsName = DNSRow.getDomainfromRow(rowIns)
        rowTS = DNSRow.getTSfromRow(rowIns)
        if DNSRow.isQuery(rowIns):
            print "IP: %s Q: %s, TS: %d, TYPE: %d" %(inet_to_str(inetsrc), dnsName, rowTS, query.qd[0].type)
        else:
            print "IP: %s A: %s, TS: %d, TYPE: %d, TTL: %d" %(inet_to_str(inetsrc), dnsName, rowTS, query.qd[0].type, query.an[0].ttl)

    @staticmethod
    def getTarget(rowIns):
        from constants import L3SRC,L3DST, L4DST
        (dFile,idx) = rowIns
        if dFile[idx][L4DST] == 53:
            src = dFile[idx][L3SRC]
        else:
            src = dFile[idx][L3DST]
        return src

    @staticmethod
    def getDomainfromRow(rowIns):
        (dFile,idx) = rowIns
        query = DNSRow.getDNSData(rowIns) 
        return query.qd[0].name

    @staticmethod
    def isQuery(rowIns):
        (dFile,idx) = rowIns
        query = DNSRow.getDNSData(rowIns) 
        return len(query.an) == 0

    @staticmethod
    def extract(curRow):
        from constants import L4DST,L4SRC
        if curRow[L4DST] == 53 or curRow[L4SRC] == 53:
            return True
        return False

