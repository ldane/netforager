""" Static Util functions """

def detectDirection(src,dst):
    from constants import LOCAL,TX,RX
    if '192.168.1' in inet_to_str(src):
        if '192.168.1' in inet_to_str(dst):
            return LOCAL
        return TX
    else:
        return RX

def mac_addr(address):
    """Convert a MAC address to a readable/printable string
       Args:
           address (str): a MAC address in hex form (e.g. '\x01\x02\x03\x04\x05\x06')
       Returns:
           str: Printable/readable MAC address
    """
    return ':'.join('%02x' % ord(b) for b in address)


def inet_to_str(inet):
    """Convert inet object to a string
        Args:
            inet (inet struct): inet network address
        Returns:
            str: Printable/readable IP address
    """
    # First try ipv4 and then ipv6
    import socket
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)

def str_to_inet(item):
    """Convert string to a inet object
        Args:
            str: Printable/readable IP address
        Returns:
            inet (inet struct): inet network address
    """
    # First try ipv4 and then ipv6
    import socket
    try:
        return socket.inet_pton(socket.AF_INET, item)
    except:
        return socket.inet_pton(socket.AF_INET6, item)

def timedelta(dumpfile,rowA,rowB):
    from datetime import datetime as dt
    from dateutil.relativedelta import relativedelta
    import constants as c

    x=dt.fromtimestamp(dumpfile.data[rowA,c.TS])
    y=dt.fromtimestamp(dumpfile.data[rowB,c.TS])

    rd = relativedelta(y,x)
    return rd


def isSorted(iterable, compare=None):
    from itertools import imap, tee
    import operator
   
    if compare is None:
        compare = operator.le

    a, b = tee(iterable)
    next(b, None)
    return all(imap(compare, a, b))
