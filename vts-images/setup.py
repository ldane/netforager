# Copyright (c) 2018  Levent Dane.

from setuptools import setup, find_packages

setup(name = 'netuser-images',
      version = '1.0.0',
      author_email = 'leventdane@gmail.com',
      packages = find_packages(),
      install_requires = [
        ],
      classifiers = [
        "Programming Language :: Python :: 2.7",
        "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
        ]
      )
