#!/usr/bin/env python

# Copyright (c) 2016-2017  Barnstormer Softworks, Ltd.

import argparse
import json
import os
import os.path
import subprocess
import sys

IMAGE_BASE = "/opt/foam/docker-imagespecs/_basespecs"

def parse_args ():
  parser = argparse.ArgumentParser()
  parser.add_argument("--force", dest="force", action="store_true", default=False)
  parser.add_argument("name", nargs='+')
  opts = parser.parse_args()
  return opts


ENVS = ["HTTPS_PROXY", "HTTP_PROXY", "PIP_INDEX_URL"]
def buildEnv (repl_vars):
  env = {}
  for name in ENVS:
    if os.environ.get(name):
      env[name.lower()] = os.environ.get(name)
    elif os.environ.get(name.lower()):
      env[name.lower()] = os.environ.get(name)

  ENVSTR = ""
  if env:
    ENVSTR = "ENV %s" % (" ".join(["%s=%s" % (k,v) for k,v in env.iteritems()]))
  repl_vars["ENV"] = ENVSTR
  

def build (opts):
  repl_vars = {}
  buildEnv(repl_vars)

  for name in opts.name:
    for adname in os.listdir("."):
      if not os.path.isdir("%s/%s" % (os.getcwd(), adname)):
        continue
      if adname.startswith("."):
        continue
      if adname != name:
        continue

      img_name = adname
      specpath = "%s/runtime/spec.json" % (adname)
      spectarget = "%s/%s/spec.json" % (IMAGE_BASE, adname)

      obj = json.loads(open(specpath, "r").read())
      try:
        img_name = obj["image-name"]
      except Exception:
        pass

      build = True
      try:
        tobj = json.loads(open(spectarget, "r").read())
        target_img_name = tobj["image-name"]
        if target_img_name == img_name:
          build = False
      except Exception:
        pass

      print "Copying specfile %s" % (specpath)
      subprocess.call("sudo -u www-data mkdir %s/%s" % (IMAGE_BASE, adname), shell=True)
      subprocess.call("sudo -u www-data cp %s %s" % (specpath, spectarget), shell=True)

      if opts.force:
        subprocess.call("sudo docker rmi %s" % (img_name), shell=True)
        build = True

      if build:
        print "Building %s as %s" % (adname, img_name)
        if os.path.exists("%s/build/docker.spec" % (adname)):
          with open("%s/build/Dockerfile" % (adname)) as df:
            data = open("%s/build/docker.spec" % (adname), "r").read()
            df.write(data % (repl_vars))
          subprocess.call("sudo docker build -t %s %s/build/" % (img_name, adname), shell=True)
          os.unlink("%s/build/Dockerfile" % (adname))
        else:
          subprocess.call("sudo docker build -t %s %s/build/" % (img_name, adname), shell=True)
      else:
        print "No need to rebuild image"


if __name__ == '__main__':
  opts = parse_args()
  build(opts)
